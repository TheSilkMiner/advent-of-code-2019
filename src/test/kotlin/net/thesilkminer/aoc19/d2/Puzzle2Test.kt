package net.thesilkminer.aoc19.d2

import org.junit.Assert
import org.junit.Test

class Puzzle2Test {
    @Test
    fun testFoundInputsForValues() {
        findInputsForOutput(18, 1, 10, listOf(1, 0, 0, 0, 99, 800, 2, 48, 8, 9, 10)).apply {
            Assert.assertEquals(Pair(2, 9), this)
        }
        findInputsForOutput(18, 1, 10, listOf(2, 0, 0, 0, 99, 800, 2, 48, 8, 9, 10)).apply {
            Assert.assertEquals(Pair(6, 9), this)
        }
    }

    @Test(expected = NoInputAvailableException::class)
    fun testNoSuitableOutput() {
        findInputsForOutput(800, 1, 3, listOf(1, 0, 0, 0, 99))
    }
}
