package net.thesilkminer.aoc19.d2

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test(expected = UnknownOpCodeException::class)
    fun testUnknownOpCodeException() {
        IntCodeCpu.run(listOf(48, 99))
    }

    @Test
    fun testHaltNotPropagating() {
        IntCodeCpu.run(listOf(99))
        Assert.assertTrue(true)
    }

    @Test
    fun testIntCodePrograms() {
        IntCodeCpu.run(listOf(1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50)).apply {
            Assert.assertEquals(3_500, this[0])
        }
        IntCodeCpu.run(listOf(1, 0, 0, 0, 99)).apply {
            Assert.assertEquals(2, this[0])
        }
        IntCodeCpu.run(listOf(2, 3, 0, 3, 99)).apply {
            Assert.assertEquals(6, this[3])
        }
        IntCodeCpu.run(listOf(2, 4, 4, 5, 99, 0)).apply {
            Assert.assertEquals(9_801, this[5])
        }
        IntCodeCpu.run(listOf(1, 1, 1, 4, 99, 5, 6, 0, 99)).apply {
            Assert.assertEquals(2, this[4])
            Assert.assertEquals(30, this[0])
        }
    }

    @Test
    fun testStringToIntCodeProgramConversion() {
        IntCodeCpu.run("1,0,0,0,99").apply {
            Assert.assertEquals(2, this[0])
        }
    }
}
