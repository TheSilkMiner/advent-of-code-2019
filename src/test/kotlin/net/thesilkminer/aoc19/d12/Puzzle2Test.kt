package net.thesilkminer.aoc19.d12

import org.junit.Assert
import org.junit.Test

class Puzzle2Test {
    @Test
    fun testFindPreviousState() {
        Assert.assertEquals(2_772, checkForPreviousStates(listOf("<x=-1, y=0, z=2>", "<x=2, y=-10, z=-7>", "<x=4, y=-8, z=8>", "<x=3, y=5, z=-1>")))
        Assert.assertEquals(4_686_774_924, checkForPreviousStates(listOf("<x=-8, y=-10, z=0>", "<x=5, y=5, z=10>", "<x=2, y=-7, z=3>", "<x=9, y=-8, z=-3>")))
    }
}
