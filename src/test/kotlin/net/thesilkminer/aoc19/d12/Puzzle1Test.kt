package net.thesilkminer.aoc19.d12

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test
    fun testCelestialBodyConversion() {
        CelestialBody.fromVectorData("<x=-1, y=0, z=2>").apply {
            Assert.assertEquals(Vector3(-1, 0, 2), this.position)
            Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
        }
        CelestialBody.fromVectorData("<x=2, y=1, z=-3>").apply {
            Assert.assertEquals(Vector3(2, 1, -3), this.position)
            Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
        }
        CelestialBody.fromVectorData("<x=-8, y=-10, z=0>").apply {
            Assert.assertEquals(Vector3(-8, -10, 0), this.position)
            Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
        }
    }

    @Test
    fun testMotionSimulation() {
        val data = listOf("<x=-1, y=0, z=2>", "<x=2, y=-10, z=-7>", "<x=4, y=-8, z=8>", "<x=3, y=5, z=-1>")
        simulateSpaceForTicks(0, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(-1, 0, 2))?.apply {
                Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, -10, -7))?.apply {
                Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(4, -8, 8))?.apply {
                Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(3, 5, -1))?.apply {
                Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(1, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(2, -1, 1))?.apply {
                Assert.assertEquals(Vector3(3, -1, -1), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(3, -7, -4))?.apply {
                Assert.assertEquals(Vector3(1, 3, 3), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(1, -7, 5))?.apply {
                Assert.assertEquals(Vector3(-3, 1, -3), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, 2, 0))?.apply {
                Assert.assertEquals(Vector3(-1, -3, 1), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(2, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(5, -3, -1))?.apply {
                Assert.assertEquals(Vector3(3, -2, -2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(1, -2, 2))?.apply {
                Assert.assertEquals(Vector3(-2, 5, 6), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(1, -4, -1))?.apply {
                Assert.assertEquals(Vector3(0, 3, -6), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(1, -4, 2))?.apply {
                Assert.assertEquals(Vector3(-1, -6, 2), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(3, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(5, -6, -1))?.apply {
                Assert.assertEquals(Vector3(0, -3, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(0, 0, 6))?.apply {
                Assert.assertEquals(Vector3(-1, 2, 4), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, 1, -5))?.apply {
                Assert.assertEquals(Vector3(1, 5, -4), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(1, -8, 2))?.apply {
                Assert.assertEquals(Vector3(0, -4, 0), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(4, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(2, -8, 0))?.apply {
                Assert.assertEquals(Vector3(-3, -2, 1), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, 1, 7))?.apply {
                Assert.assertEquals(Vector3(2, 1, 1), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, 3, -6))?.apply {
                Assert.assertEquals(Vector3(0, 2, -1), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, -9, 1))?.apply {
                Assert.assertEquals(Vector3(1, -1, -1), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(5, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(-1, -9, 2))?.apply {
                Assert.assertEquals(Vector3(-3, -1, 2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(4, 1, 5))?.apply {
                Assert.assertEquals(Vector3(2, 0, -2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, 2, -4))?.apply {
                Assert.assertEquals(Vector3(0, -1, 2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(3, -7, -1))?.apply {
                Assert.assertEquals(Vector3(1, 2, -2), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(6, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(-1, -7, 3))?.apply {
                Assert.assertEquals(Vector3(0, 2, 1), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(3, 0, 0))?.apply {
                Assert.assertEquals(Vector3(-1, -1, -5), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(3, -2, 1))?.apply {
                Assert.assertEquals(Vector3(1, -4, 5), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(3, -4, -2))?.apply {
                Assert.assertEquals(Vector3(0, 3, -1), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(7, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(2, -2, 1))?.apply {
                Assert.assertEquals(Vector3(3, 5, -2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(1, -4, -4))?.apply {
                Assert.assertEquals(Vector3(-2, -4, -4), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(3, -7, 5))?.apply {
                Assert.assertEquals(Vector3(0, -5, 4), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, 0, 0))?.apply {
                Assert.assertEquals(Vector3(-1, 4, 2), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(8, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(5, 2, -2))?.apply {
                Assert.assertEquals(Vector3(3, 4, -3), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, -7, -5))?.apply {
                Assert.assertEquals(Vector3(1, -3, -1), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(0, -9, 6))?.apply {
                Assert.assertEquals(Vector3(-3, -2, 1), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(1, 1, 3))?.apply {
                Assert.assertEquals(Vector3(-1, 1, 3), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(9, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(5, 3, -4))?.apply {
                Assert.assertEquals(Vector3(0, 1, -2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, -9, -3))?.apply {
                Assert.assertEquals(Vector3(0, -2, 2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(0, -8, 4))?.apply {
                Assert.assertEquals(Vector3(0, 1, -2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(1, 1, 5))?.apply {
                Assert.assertEquals(Vector3(0, 0, 2), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(10, data).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(2, 1, -3))?.apply {
                Assert.assertEquals(Vector3(-3, -2, 1), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(1, -8, 0))?.apply {
                Assert.assertEquals(Vector3(-1, 1, 3), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(3, -6, 1))?.apply {
                Assert.assertEquals(Vector3(3, 2, -3), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, 0, 4))?.apply {
                Assert.assertEquals(Vector3(1, -1, -1), this.velocity)
            } != null)
        }

        val secondData = listOf("<x=-8, y=-10, z=0>", "<x=5, y=5, z=10>", "<x=2, y=-7, z=3>", "<x=9, y=-8, z=-3>")
        simulateSpaceForTicks(0, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(-8, -10, 0))?.apply {
                Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(5, 5, 10))?.apply {
                Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, -7, 3))?.apply {
                Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(9, -8, -3))?.apply {
                Assert.assertEquals(Vector3(0, 0, 0), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(10, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(-9, -10, 1))?.apply {
                Assert.assertEquals(Vector3(-2, -2, -1), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(4, 10, 9))?.apply {
                Assert.assertEquals(Vector3(-3, 7, -2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(8, -10, -3))?.apply {
                Assert.assertEquals(Vector3(5, -1, -2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(5, -10, 3))?.apply {
                Assert.assertEquals(Vector3(0, -4, 5), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(20, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(-10, 3, -4))?.apply {
                Assert.assertEquals(Vector3(-5, 2, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(5, -25, 6))?.apply {
                Assert.assertEquals(Vector3(1, 1, -4), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(13, 1, 1))?.apply {
                Assert.assertEquals(Vector3(5, -2, 2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(0, 1, 7))?.apply {
                Assert.assertEquals(Vector3(-1, -1, 2), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(30, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(15, -6, -9))?.apply {
                Assert.assertEquals(Vector3(-5, 4, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-4, -11, 3))?.apply {
                Assert.assertEquals(Vector3(-3, -10, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(0, -1, 11))?.apply {
                Assert.assertEquals(Vector3(7, 4, 3), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-3, -2, 5))?.apply {
                Assert.assertEquals(Vector3(1, 2, -3), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(40, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(14, -12, -4))?.apply {
                Assert.assertEquals(Vector3(11, 3, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-1, 18, 8))?.apply {
                Assert.assertEquals(Vector3(-5, 2, 3), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-5, -14, 8))?.apply {
                Assert.assertEquals(Vector3(1, -2, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(0, -12, -2))?.apply {
                Assert.assertEquals(Vector3(-7, -3, -3), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(50, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(-23, 4, 1))?.apply {
                Assert.assertEquals(Vector3(-7, -1, 2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(20, -31, 13))?.apply {
                Assert.assertEquals(Vector3(5, 3, 4), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-4, 6, 1))?.apply {
                Assert.assertEquals(Vector3(-1, 1, -3), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(15, 1, -5))?.apply {
                Assert.assertEquals(Vector3(3, -3, -3), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(60, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(36, -10, 6))?.apply {
                Assert.assertEquals(Vector3(5, 0, 3), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-18, 10, 9))?.apply {
                Assert.assertEquals(Vector3(-3, -7, 5), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(8, -12, -3))?.apply {
                Assert.assertEquals(Vector3(-2, 1, -7), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-18, -8, -2))?.apply {
                Assert.assertEquals(Vector3(0, 6, -1), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(70, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(-33, -6, 5))?.apply {
                Assert.assertEquals(Vector3(-5, -4, 7), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(13, -9, 2))?.apply {
                Assert.assertEquals(Vector3(-2, 11, 3), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(11, -8, 2))?.apply {
                Assert.assertEquals(Vector3(8, -6, -7), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(17, 3, 1))?.apply {
                Assert.assertEquals(Vector3(-1, -1, -3), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(80, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(30, -8, 3))?.apply {
                Assert.assertEquals(Vector3(3, 3, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-2, -4, 0))?.apply {
                Assert.assertEquals(Vector3(4, -13, 2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-18, -7, 15))?.apply {
                Assert.assertEquals(Vector3(-8, 2, -2), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-2, -1, -8))?.apply {
                Assert.assertEquals(Vector3(1, 8, 0), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(90, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(-25, -1, 4))?.apply {
                Assert.assertEquals(Vector3(1, -3, 4), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(2, -9, 0))?.apply {
                Assert.assertEquals(Vector3(-3, 13, -1), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(32, -8, 14))?.apply {
                Assert.assertEquals(Vector3(5, -4, 6), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-1, -2, -8))?.apply {
                Assert.assertEquals(Vector3(-3, -6, -9), this.velocity)
            } != null)
        }
        simulateSpaceForTicks(100, secondData).apply {
            Assert.assertTrue(this.getBodyAt(Vector3(8, -12, -9))?.apply {
                Assert.assertEquals(Vector3(-7, 3, 0), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(13, 16, -3))?.apply {
                Assert.assertEquals(Vector3(3, -11, -5), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(-29, -11, -1))?.apply {
                Assert.assertEquals(Vector3(-3, 7, 4), this.velocity)
            } != null)
            Assert.assertTrue(this.getBodyAt(Vector3(16, -13, 23))?.apply {
                Assert.assertEquals(Vector3(7, 1, 1), this.velocity)
            } != null)
        }
    }

    @Test
    fun testSystemEnergy() {
        val data = listOf("<x=-1, y=0, z=2>", "<x=2, y=-10, z=-7>", "<x=4, y=-8, z=8>", "<x=3, y=5, z=-1>")
        Assert.assertEquals(179, simulateSpaceForTicks(10, data).findEnergy())

        val secondData = listOf("<x=-8, y=-10, z=0>", "<x=5, y=5, z=10>", "<x=2, y=-7, z=3>", "<x=9, y=-8, z=-3>")
        Assert.assertEquals(1_940, simulateSpaceForTicks(100, secondData).findEnergy())
    }
}
