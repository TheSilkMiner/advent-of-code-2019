package net.thesilkminer.aoc19.d13

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test
    fun testTileTypeConversion() {
        Assert.assertEquals(TileType.EMPTY, 0.toTileType())
        Assert.assertEquals(TileType.WALL, 1.toTileType())
        Assert.assertEquals(TileType.BLOCK, 2.toTileType())
        Assert.assertEquals(TileType.HORIZONTAL_PADDLE, 3.toTileType())
        Assert.assertEquals(TileType.BALL, 4.toTileType())
    }

    @Test(expected = IllegalArgumentException::class)
    fun testIllegalTileTypeConversion() {
        (-1).toTileType()
    }

    // The rest is really tightly coupled to the implementation, so I don't know what tests I could be writing
}
