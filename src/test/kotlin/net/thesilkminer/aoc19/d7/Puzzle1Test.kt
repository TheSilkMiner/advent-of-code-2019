package net.thesilkminer.aoc19.d7

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test
    fun testRunProgramFor() {
        Assert.assertEquals(11, listOf(3, 0, 3, 1, 1, 0, 1, 0, 4, 0, 99).runProgramFor(Tuple5(1, 1, 2, 3, 4)))
        Assert.assertEquals(0, listOf(3, 0, 3, 1, 1, 0, 1, 0, 4, 0, 99).runProgramFor(Tuple5(0, 0, 0, 0, 0)))
        Assert.assertEquals(0, listOf(3, 0, 3, 1, 2, 0, 1, 3, 4, 3, 99).runProgramFor(Tuple5(8, -2, 8, -2, 8)))
        Assert.assertEquals(2_168, listOf(3, 0, 3, 1, 1001, 1, 1, 1, 2, 0, 1, 1, 4, 1, 99).runProgramFor(Tuple5(8, -2, 8, -2, 8)))
    }

    @Test
    fun testOutputThrusterSignals() {
        findMaxWithinInputRange(listOf(3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0), 0, 4).apply {
            Assert.assertEquals(Tuple5(4, 3, 2, 1, 0), this.first)
            Assert.assertEquals(43_210, this.second)
        }
        findMaxWithinInputRange(listOf(3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23, 23, 4, 23, 99, 0, 0), 0, 4).apply {
            Assert.assertEquals(Tuple5(0, 1, 2, 3, 4), this.first)
            Assert.assertEquals(54_321, this.second)
        }
        findMaxWithinInputRange(
            listOf(3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33, 1002, 33, 7, 33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0),
            0,
            4).apply {
            Assert.assertEquals(Tuple5(1, 0, 4, 3, 2), this.first)
            Assert.assertEquals(65_210, this.second)
        }
    }
}
