package net.thesilkminer.aoc19.d6

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test
    fun testLegalMapCreation() {
        val mapData = listOf("COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L")
        val map = UniversalOrbitMap(mapData)

        val com = map.centerOfMass
        Assert.assertEquals("COM", com.name)
        Assert.assertEquals(1, com.directOrbits.count())

        val b = com.directOrbits[0]
        Assert.assertEquals("B", b.name)
        Assert.assertEquals(1, b.depthId)
        Assert.assertEquals(2, b.directOrbits.count())
        Assert.assertTrue(b.directOrbit == com)
    }

    @Test
    fun testFindingObject() {
        val mapData = listOf("COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L")
        val map = UniversalOrbitMap(mapData)
        val jMaybe = map.find("J")
        Assert.assertTrue(jMaybe != null)
        val j = jMaybe!!
        Assert.assertEquals("J", j.name)
        Assert.assertEquals(1, j.directOrbits.count())
        Assert.assertEquals("K", j.directOrbits[0].name)
        Assert.assertTrue(j.directOrbit != null)
        Assert.assertEquals("E", j.directOrbit!!.name)
    }

    @Test(expected = IllegalStateException::class)
    fun testMultipleCenterOfMass() {
        UniversalOrbitMap(listOf("COM1)B", "B)C", "COM2)D"))
    }

    @Test(expected = IllegalStateException::class)
    fun testMultipleCentersForObject() {
        UniversalOrbitMap(listOf("COM)B", "B)C", "COM)D", "D)C"))
    }

    @Test
    fun testDepth() {
        val mapData = listOf("COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L")
        val map = UniversalOrbitMap(mapData)
        val c = map.find("C")!!
        val g = map.find("G")!!
        val k = map.find("K")!!
        Assert.assertEquals(2, c.depthId)
        Assert.assertEquals(2, g.depthId)
        Assert.assertEquals(6, k.depthId)
    }

    @Test
    fun testTotalOrbits() {
        Assert.assertEquals(6, calculateTotalOrbits(UniversalOrbitMap(listOf("COM)B", "B)C", "C)D"))))
        Assert.assertEquals(28, calculateTotalOrbits(UniversalOrbitMap(listOf("COM)B", "B)C", "C)D", "D)E", "E)J", "J)K", "K)L"))))
    }
}
