package net.thesilkminer.aoc19.d6

import org.junit.Assert
import org.junit.Test

class Puzzle2Test {
    @Test
    fun testSubMapBuild() {
        val mapData = listOf("COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L")
        val orbitingObject = UniversalOrbitMap(mapData).find("G")!!
        val subMap = orbitingObject.buildSubMap()
        Assert.assertEquals("COM", subMap.centerOfMass.name)
        Assert.assertEquals(1, subMap.centerOfMass.directOrbits[0].directOrbits.count())
        Assert.assertEquals("G", subMap.centerOfMass.directOrbits[0].directOrbits[0].name)
    }

    @Test
    fun testCalculateLastIntersectionWith() {
        val mapOne = UniversalOrbitMap(listOf("COM)A", "A)B", "B)C"))
        val mapTwo = UniversalOrbitMap(listOf("COM)A", "A)B", "B)D"))
        Assert.assertEquals("B", mapOne.calculateLastIntersectionWith(mapTwo).name)

        val mapThree = UniversalOrbitMap(listOf("COM)A", "A)B"))
        Assert.assertEquals("B", mapOne.calculateLastIntersectionWith(mapThree).name)

        val mapFour = UniversalOrbitMap(listOf("COM)K", "K)A", "A)B", "A)C"))
        Assert.assertEquals("COM", mapOne.calculateLastIntersectionWith(mapFour).name)
    }

    @Test
    fun testStepCount() {
        val map = UniversalOrbitMap(listOf("COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L", "K)YOU", "I)SAN"))
        Assert.assertEquals(4, calculateStepsForOrbitalTransfer(map))
    }
}
