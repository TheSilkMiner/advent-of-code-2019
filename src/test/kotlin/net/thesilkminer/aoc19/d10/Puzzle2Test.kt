package net.thesilkminer.aoc19.d10

import org.junit.Assert
import org.junit.Test

class Puzzle2Test {
    @Test
    fun testBasicVaporizationResults() {
        val map = Map.fromMapData("""
            .#....#####...#..
            ##...##.#####..##
            ##...#...#.#####.
            ..#.....#...###..
            ..#.#.....#....##
        """.trimIndent().split('\n'))
        val vaporizerLocation = map[8, 3]
        val explosionData = listOf(
            Pair(8, 1), Pair(9, 0), Pair(9, 1), Pair(10, 0), Pair(9, 2), Pair(11, 1), Pair(12, 1), Pair(11, 2),
            Pair(15, 1), Pair(12, 2), Pair(13, 2), Pair(14, 2), Pair(15, 2), Pair(12, 3), Pair(16, 4), Pair(15, 4),
            Pair(10, 4), Pair(4, 4), Pair(2, 4), Pair(2, 3), Pair(0, 2), Pair(1, 2), Pair(0, 1), Pair(1, 1), Pair(5, 2),
            Pair(1, 0), Pair(5, 1), Pair(6, 1), Pair(6, 0), Pair(7, 0), Pair(8, 0), Pair(10, 1), Pair(14, 0),
            Pair(16, 1), Pair(13, 3), Pair(14, 3)
        )
        explosionData.forEachIndexed { i, data ->
            vaporizeAsteroidsUntil(i + 1, map, vaporizerLocation).apply {
                Assert.assertEquals("On pair ${i + 1} $data", data.first, this.x)
                Assert.assertEquals("On pair ${i + 1} $data", data.second, this.y)
            }
        }
    }

    @Test
    fun testVaporizationResults() {
        val map = Map.fromMapData("""
            .#..##.###...#######
            ##.############..##.
            .#.######.########.#
            .###.#######.####.#.
            #####.##.#.##.###.##
            ..#####..#.#########
            ####################
            #.####....###.#.#.##
            ##.#################
            #####.##.###..####..
            ..######..##.#######
            ####.##.####...##..#
            .#####..#.######.###
            ##...#.##########...
            #.##########.#######
            .####.#.###.###.#.##
            ....##.##.###..#####
            .#.#.###########.###
            #.#.#.#####.####.###
            ###.##.####.##.#..##
        """.trimIndent().split('\n'))
        val vaporizerLocation = findBestLocation(map).first
        vaporizeAsteroidsUntil(1, map,  vaporizerLocation).apply {
            Assert.assertEquals(11, this.x)
            Assert.assertEquals(12, this.y)
        }
        vaporizeAsteroidsUntil(2, map,  vaporizerLocation).apply {
            Assert.assertEquals(12, this.x)
            Assert.assertEquals(1, this.y)
        }
        vaporizeAsteroidsUntil(3, map,  vaporizerLocation).apply {
            Assert.assertEquals(12, this.x)
            Assert.assertEquals(2, this.y)
        }
        vaporizeAsteroidsUntil(10, map,  vaporizerLocation).apply {
            Assert.assertEquals(12, this.x)
            Assert.assertEquals(8, this.y)
        }
        vaporizeAsteroidsUntil(20, map,  vaporizerLocation).apply {
            Assert.assertEquals(16, this.x)
            Assert.assertEquals(0, this.y)
        }
        vaporizeAsteroidsUntil(50, map,  vaporizerLocation).apply {
            Assert.assertEquals(16, this.x)
            Assert.assertEquals(9, this.y)
        }
        vaporizeAsteroidsUntil(100, map,  vaporizerLocation).apply {
            Assert.assertEquals(10, this.x)
            Assert.assertEquals(16, this.y)
        }
        vaporizeAsteroidsUntil(199, map,  vaporizerLocation).apply {
            Assert.assertEquals(9, this.x)
            Assert.assertEquals(6, this.y)
        }
        vaporizeAsteroidsUntil(200, map,  vaporizerLocation).apply {
            Assert.assertEquals(8, this.x)
            Assert.assertEquals(2, this.y)
        }
        vaporizeAsteroidsUntil(201, map,  vaporizerLocation).apply {
            Assert.assertEquals(10, this.x)
            Assert.assertEquals(9, this.y)
        }
        vaporizeAsteroidsUntil(299, map,  vaporizerLocation).apply {
            Assert.assertEquals(11, this.x)
            Assert.assertEquals(1, this.y)
        }
    }
}
