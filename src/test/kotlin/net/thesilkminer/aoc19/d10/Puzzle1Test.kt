package net.thesilkminer.aoc19.d10

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test
    fun testMapGeneration() {
        Map.fromMapData("""
            ......#.#.
            #..#.#....
            ..#######.
            .#.#.###..
            .#..#.....
            ..#....#.#
            #..#....#.
            .##.#..###
            ##...#..#.
            .#....####
        """.trimIndent().split('\n')).apply {
            Assert.assertEquals(10, this.xSize)
            Assert.assertEquals(10, this.ySize)
            Assert.assertEquals(TileType.ASTEROID, this[0, 1].tileType)
            Assert.assertEquals(TileType.ASTEROID, this[5, 3].tileType)
            Assert.assertEquals(TileType.ASTEROID, this[9, 5].tileType)
            Assert.assertEquals(TileType.EMPTY_SPACE, this[0, 0].tileType)
            Assert.assertEquals(TileType.EMPTY_SPACE, this[3, 7].tileType)
            Assert.assertEquals(TileType.EMPTY_SPACE, this[5, 9].tileType)
        }
    }

    @Test
    fun testPositionsDiscovery() {
        val mapData = """
            .#..#
            .....
            #####
            ....#
            ...##
        """.trimIndent().split('\n')
        val map = Map.fromMapData(mapData)
        val expectedList = listOf(
            Pair(map[1, 0], 7), Pair(map[4, 0], 7),
            Pair(map[0, 2], 6), Pair(map[1, 2], 7), Pair(map[2, 2], 7), Pair(map[3, 2], 7), Pair(map[4, 2], 5),
            Pair(map[4, 3], 7),
            Pair(map[3, 4], 8), Pair(map[4, 4], 7)
        )
        findAllLocations(map).apply {
            expectedList.forEach {
                Assert.assertEquals("For tile ${it.first}", it.second, this.find { other -> it.first == other.first }!!.second)
            }
        }
    }

    @Test
    fun testBestPositionDiscovery() {
        findBestLocation(Map.fromMapData("""
            .#..#
            .....
            #####
            ....#
            ...##
        """.trimIndent().split('\n'))).apply {
            Assert.assertEquals(3, this.first.x)
            Assert.assertEquals(4, this.first.y)
            Assert.assertEquals(8, this.second)
        }

        findBestLocation(Map.fromMapData("""
            ......#.#.
            #..#.#....
            ..#######.
            .#.#.###..
            .#..#.....
            ..#....#.#
            #..#....#.
            .##.#..###
            ##...#..#.
            .#....####
        """.trimIndent().split('\n'))).apply {
            Assert.assertEquals(5, this.first.x)
            Assert.assertEquals(8, this.first.y)
            Assert.assertEquals(33, this.second)
        }

        findBestLocation(Map.fromMapData("""
            #.#...#.#.
            .###....#.
            .#....#...
            ##.#.#.#.#
            ....#.#.#.
            .##..###.#
            ..#...##..
            ..##....##
            ......#...
            .####.###.
        """.trimIndent().split('\n'))).apply {
            Assert.assertEquals(1, this.first.x)
            Assert.assertEquals(2, this.first.y)
            Assert.assertEquals(35, this.second)
        }

        findBestLocation(Map.fromMapData("""
            .#..#..###
            ####.###.#
            ....###.#.
            ..###.##.#
            ##.##.#.#.
            ....###..#
            ..#.#..#.#
            #..#.#.###
            .##...##.#
            .....#.#..
        """.trimIndent().split('\n'))).apply {
            Assert.assertEquals(6, this.first.x)
            Assert.assertEquals(3, this.first.y)
            Assert.assertEquals(41, this.second)
        }

        findBestLocation(Map.fromMapData("""
            .#..##.###...#######
            ##.############..##.
            .#.######.########.#
            .###.#######.####.#.
            #####.##.#.##.###.##
            ..#####..#.#########
            ####################
            #.####....###.#.#.##
            ##.#################
            #####.##.###..####..
            ..######..##.#######
            ####.##.####...##..#
            .#####..#.######.###
            ##...#.##########...
            #.##########.#######
            .####.#.###.###.#.##
            ....##.##.###..#####
            .#.#.###########.###
            #.#.#.#####.####.###
            ###.##.####.##.#..##
        """.trimIndent().split('\n'))).apply {
            Assert.assertEquals(11, this.first.x)
            Assert.assertEquals(13, this.first.y)
            Assert.assertEquals(210, this.second)
        }
    }
}
