package net.thesilkminer.aoc19.d1

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test
    fun testCalculateFuel() {
        Assert.assertEquals(2, calculateFuel(12))
        Assert.assertEquals(2, calculateFuel(14))
        Assert.assertEquals(654, calculateFuel(1_969))
        Assert.assertEquals(33_583, calculateFuel(100_756))
    }

    @Test
    fun testCalculateTotalFuel() {
        Assert.assertEquals(0, calculateTotalFuel(listOf()))
        Assert.assertEquals(34_241, calculateTotalFuel(listOf(12, 14, 1_969, 100_756)))
    }
}
