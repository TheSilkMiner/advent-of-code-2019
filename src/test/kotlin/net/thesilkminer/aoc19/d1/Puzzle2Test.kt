package net.thesilkminer.aoc19.d1

import org.junit.Assert
import org.junit.Test

class Puzzle2Test {
    @Test
    fun calculateRealFuelTest() {
        Assert.assertEquals(2, calculateRealFuel(14))
        Assert.assertEquals(966, calculateRealFuel(1_969))
        Assert.assertEquals(50_346, calculateRealFuel(100_756))
    }

    @Test
    fun calculateRealTotalFuelTest() {
        Assert.assertEquals(0, calculateRealTotalFuel(listOf()))
        Assert.assertEquals(51_314, calculateRealTotalFuel(listOf(14, 1_969, 100_756)))
    }
}
