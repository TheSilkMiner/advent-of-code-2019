package net.thesilkminer.aoc19.d9

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test(expected = UnknownOpcodeException::class)
    fun testUnknownOpCodeException() {
        IntCodeComputer.executeSyncProgram(listOf(44, 99))
    }

    @Test
    fun testHaltNotPropagating() {
        IntCodeComputer.executeSyncProgram(listOf(99))
        Assert.assertTrue(true)
    }

    @Test
    fun testBasicIntCodePrograms() {
        IntCodeComputer.executeSyncProgram(listOf(1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50)).memory.apply {
            Assert.assertEquals(3_500L, this[0])
        }
        IntCodeComputer.executeSyncProgram(listOf(1, 0, 0, 0, 99)).memory.apply {
            Assert.assertEquals(2L, this[0])
        }
        IntCodeComputer.executeSyncProgram(listOf(2, 3, 0, 3, 99)).memory.apply {
            Assert.assertEquals(6L, this[3])
        }
        IntCodeComputer.executeSyncProgram(listOf(2, 4, 4, 5, 99, 0)).memory.apply {
            Assert.assertEquals(9_801L, this[5])
        }
        IntCodeComputer.executeSyncProgram(listOf(1, 1, 1, 4, 99, 5, 6, 0, 99)).memory.apply {
            Assert.assertEquals(2L, this[4])
            Assert.assertEquals(30L, this[0])
        }
    }

    @Test
    fun testPositionalParametersIntCodePrograms() {
        IntCodeComputer.executeSyncProgram(listOf(1002, 4, 3, 4, 33)).memory.apply {
            Assert.assertEquals(99L, this[4])
        }
        IntCodeComputer.executeSyncProgram(listOf(1101, 100, -1, 4, 0)).memory.apply {
            Assert.assertEquals(99L, this[4])
        }
    }

    @Test
    fun testRelationParametersIntCodePrograms() {
        val quineList = mutableListOf<Long>()
        val quine = IntCodeComputer.executeProgram(listOf(109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99)) { quineList += it }
        val sixteenDigit = IntCodeComputer.executeProgram(listOf(1102, 34915192, 34915192, 7, 4, 7, 99, 0)) {
            Assert.assertEquals(1_219_070_632_396_864L, it)
        }
        IntCodeComputer.executeProgram(listOf(104, 1125899906842624, 99)) {
            Assert.assertEquals(1_125_899_906_842_624L, it)
        }
        sixteenDigit.get().memory.apply {
            Assert.assertEquals(1_219_070_632_396_864L, this[7])
        }
        quine.get().apply {
            Assert.assertEquals(16, quineList.count())
            val target = listOf(109L, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99)
            target.forEachIndexed { i, value -> Assert.assertEquals(value, quineList[i]) }
        }
    }

    @Test
    fun testIndependentParallelProcessing() {
        val long = IntCodeComputer.executeProgram(listOf(101, 1, 100, 100, 108, 999_999, 100, 101, 1006, 101, 0, 99))
        val short = IntCodeComputer.executeProgram(listOf(1, 1, 1, 1, 99)) { }
        short.get().memory.apply {
            Assert.assertEquals(2L, this[1])
        }
        long.get().memory.apply {
            Assert.assertEquals(999_999L, this[100])
            Assert.assertEquals(1L, this[101])
        }
    }

    @Test
    fun testUnboundedMemoryAccess() {
        val test = mutableListOf<Long>()
        IntCodeComputer.executeSyncProgram(listOf(4, 1_000_000_000, 99)) { test += it }
        Assert.assertEquals(0, test[0])
    }

    @Test(expected = IllegalArgumentException::class)
    fun testAccessInvalidMemory() {
        try {
            IntCodeComputer.executeSyncProgram(listOf(1, 1, -1, 1, 99))
        } catch (e: Exception) {
            throw e.cause ?: e
        }
    }

    @Test
    fun testInputOutputIntCodePrograms() {
        val list = mutableListOf<Long>()
        IntCodeComputer.executeSyncProgram(listOf(3, 0, 4, 0, 99), sequenceOf(44L)) { list += it }
        Assert.assertEquals(1, list.count())
        Assert.assertEquals(44L, list[0])
    }
}
