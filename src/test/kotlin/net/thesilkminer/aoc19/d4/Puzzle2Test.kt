package net.thesilkminer.aoc19.d4

import org.junit.Assert
import org.junit.Test

class Puzzle2Test {
    @Test
    fun testValidCombinations() {
        Assert.assertTrue(ensureValidCombinationWithNewCriteria(122_345))
        Assert.assertTrue(ensureValidCombinationWithNewCriteria(335_679))
        Assert.assertTrue(ensureValidCombinationWithNewCriteria(112_233))
        Assert.assertTrue(ensureValidCombinationWithNewCriteria(111_122))
    }

    @Test
    fun testInvalidCombinations() {
        Assert.assertFalse(ensureValidCombinationWithNewCriteria(1))
        Assert.assertFalse(ensureValidCombinationWithNewCriteria(111_111))
        Assert.assertFalse(ensureValidCombinationWithNewCriteria(111_123))
        Assert.assertFalse(ensureValidCombinationWithNewCriteria(100_000))
        Assert.assertFalse(ensureValidCombinationWithNewCriteria(135_679))
        Assert.assertFalse(ensureValidCombinationWithNewCriteria(223_450))
        Assert.assertFalse(ensureValidCombinationWithNewCriteria(123_789))
        Assert.assertFalse(ensureValidCombinationWithNewCriteria(123_444))
    }

    @Test
    fun testCountingValidCombinations() {
        Assert.assertEquals(0, calculateCombinationsWithAdditionalCriteria(0, 1))
        Assert.assertEquals(0, calculateCombinationsWithAdditionalCriteria(121_111, 122_221))
        Assert.assertEquals(1, calculateCombinationsWithAdditionalCriteria(123_454, 123_457))
        Assert.assertEquals(1, calculateCombinationsWithAdditionalCriteria(111_111, 111_122))
        Assert.assertEquals(2, calculateCombinationsWithAdditionalCriteria(111_111, 111_133))
    }
}
