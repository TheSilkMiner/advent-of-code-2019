package net.thesilkminer.aoc19.d4

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test
    fun testValidCombinations() {
        Assert.assertTrue(ensureValidCombination(111_111))
        Assert.assertTrue(ensureValidCombination(122_345))
        Assert.assertTrue(ensureValidCombination(111_123))
        Assert.assertTrue(ensureValidCombination(335_679))
        Assert.assertTrue(ensureValidCombination(112_233))
    }

    @Test
    fun testInvalidCombinations() {
        Assert.assertFalse(ensureValidCombination(1))
        Assert.assertFalse(ensureValidCombination(100_000))
        Assert.assertFalse(ensureValidCombination(135_679))
        Assert.assertFalse(ensureValidCombination(223_450))
        Assert.assertFalse(ensureValidCombination(123_789))
    }

    @Test
    fun testCountingValidCombinations() {
        Assert.assertEquals(0, calculateCombinations(0, 1))
        Assert.assertEquals(0, calculateCombinations(121_111, 122_221))
        Assert.assertEquals(1, calculateCombinations(123_454, 123_457))
        Assert.assertEquals(10, calculateCombinations(111_111, 111_122))
    }
}
