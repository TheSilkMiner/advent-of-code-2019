package net.thesilkminer.aoc19.d8

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test
    fun testImageDecoding() {
        decodeIntoImage(sequenceOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2), Size(3, 2)).apply {
            val other = Image(listOf(
                Layer(Size(3, 2), listOf(
                    Pixel(0, 0, 1),
                    Pixel(1, 0, 2),
                    Pixel(2, 0, 3),
                    Pixel(0, 1, 4),
                    Pixel(1, 1, 5),
                    Pixel(2, 1, 6)
                )),
                Layer(Size(3, 2), listOf(
                    Pixel(0, 0, 7),
                    Pixel(1, 0, 8),
                    Pixel(2, 0, 9),
                    Pixel(0, 1, 0),
                    Pixel(1, 1, 1),
                    Pixel(2, 1, 2)
                ))
            ))
            Assert.assertEquals(other, this)
        }

        decodeIntoImage(sequenceOf(1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3), Size(4, 3)).apply {
            val other = Image(listOf(
                Layer(Size(4, 3), listOf(
                    Pixel(0, 0, 1),
                    Pixel(1, 0, 1),
                    Pixel(2, 0, 1),
                    Pixel(3, 0, 1),
                    Pixel(0, 1, 2),
                    Pixel(1, 1, 2),
                    Pixel(2, 1, 2),
                    Pixel(3, 1, 2),
                    Pixel(0, 2, 3),
                    Pixel(1, 2, 3),
                    Pixel(2, 2, 3),
                    Pixel(3, 2, 3)
                ))
            ))
            Assert.assertEquals(other, this)
        }
    }

    @Test(expected = IllegalStateException::class)
    fun testDecodeIncompleteStream() {
        decodeIntoImage(sequenceOf(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1), Size(40, 3))
    }

    @Test
    fun testPixelValueAmount() {
        Assert.assertEquals(0, Layer(Size(2, 1), listOf(Pixel(0, 0, 1), Pixel(1, 0, 1))).findPixelValueAmount(0))
        Assert.assertEquals(2, Layer(Size(2, 1), listOf(Pixel(0, 0, 1), Pixel(1, 0, 1))).findPixelValueAmount(1))
        Assert.assertEquals(1, Layer(Size(2, 1), listOf(Pixel(0, 0, 2), Pixel(1, 0, 1))).findPixelValueAmount(2))
    }

    @Test
    fun testFindFewestPixelValuesInLayers() {
        decodeIntoImage(sequenceOf(1, 1, 1, 2, 2, 3, 4, 5, 1, 1, 1, 1, 2, 3, 4, 5, 6, 7), Size(3, 2)).apply {
            Assert.assertEquals(this.layers[2], this.findFewestPixelValuesInLayers(1))
            Assert.assertEquals(this.layers[1], this.findFewestPixelValuesInLayers(2))
        }
    }

    @Test
    fun testChecksumComputation() {
        decodeIntoImage(sequenceOf(0, 1, 1, 1, 1, 0, 0, 1, 2, 1, 2, 2), Size(2, 2)).apply {
            Assert.assertEquals(3, this.computeChecksum())
        }
    }
}
