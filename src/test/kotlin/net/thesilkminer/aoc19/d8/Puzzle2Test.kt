package net.thesilkminer.aoc19.d8

import org.junit.Assert
import org.junit.Test

class Puzzle2Test {
    @Test
    fun testPixelFlattening() {
        val blackPixel = Pixel(0, 0, PixelColor.BLACK.mnemonic)
        val whitePixel = Pixel(0, 0, PixelColor.WHITE.mnemonic)
        val transparentPixel = Pixel(0, 0, PixelColor.TRANSPARENT.mnemonic)

        Assert.assertEquals(blackPixel, blackPixel.flattenOnto(blackPixel))
        Assert.assertEquals(blackPixel, blackPixel.flattenOnto(whitePixel))
        Assert.assertEquals(blackPixel, blackPixel.flattenOnto(transparentPixel))

        Assert.assertEquals(whitePixel, whitePixel.flattenOnto(blackPixel))
        Assert.assertEquals(whitePixel, whitePixel.flattenOnto(whitePixel))
        Assert.assertEquals(whitePixel, whitePixel.flattenOnto(transparentPixel))

        Assert.assertEquals(blackPixel, transparentPixel.flattenOnto(blackPixel))
        Assert.assertEquals(whitePixel, transparentPixel.flattenOnto(whitePixel))
        Assert.assertEquals(transparentPixel, transparentPixel.flattenOnto(transparentPixel))
    }

    @Test
    fun testLayerFlattening() {
        val layerOne = Layer(Size(2, 2), listOf(
            Pixel(0, 0, PixelColor.WHITE.mnemonic),
            Pixel(1, 0, PixelColor.TRANSPARENT.mnemonic),
            Pixel(0, 1, PixelColor.BLACK.mnemonic),
            Pixel(1, 1, PixelColor.TRANSPARENT.mnemonic)
        ))
        val layerTwo = Layer(Size(2, 2), listOf(
            Pixel(0, 0, PixelColor.TRANSPARENT.mnemonic),
            Pixel(1, 0, PixelColor.TRANSPARENT.mnemonic),
            Pixel(0, 1, PixelColor.WHITE.mnemonic),
            Pixel(1, 1, PixelColor.WHITE.mnemonic)
        ))
        val layerThree = Layer(Size(2, 2), listOf(
            Pixel(0, 0, PixelColor.BLACK.mnemonic),
            Pixel(1, 0, PixelColor.BLACK.mnemonic),
            Pixel(0, 1, PixelColor.BLACK.mnemonic),
            Pixel(1, 1, PixelColor.BLACK.mnemonic)
        ))
        layerOne.flattenOnto(layerTwo).apply {
            Assert.assertEquals(PixelColor.WHITE, this.pixels.find { it.x == 0 && it.y == 0 }!!.value.toPixelColor())
            Assert.assertEquals(PixelColor.TRANSPARENT, this.pixels.find { it.x == 1 && it.y == 0 }!!.value.toPixelColor())
            Assert.assertEquals(PixelColor.BLACK, this.pixels.find { it.x == 0 && it.y == 1 }!!.value.toPixelColor())
            Assert.assertEquals(PixelColor.WHITE, this.pixels.find { it.x == 1 && it.y == 1 }!!.value.toPixelColor())
        }
        layerTwo.flattenOnto(layerThree).apply {
            Assert.assertEquals(PixelColor.BLACK, this.pixels.find { it.x == 0 && it.y == 0 }!!.value.toPixelColor())
            Assert.assertEquals(PixelColor.BLACK, this.pixels.find { it.x == 1 && it.y == 0 }!!.value.toPixelColor())
            Assert.assertEquals(PixelColor.WHITE, this.pixels.find { it.x == 0 && it.y == 1 }!!.value.toPixelColor())
            Assert.assertEquals(PixelColor.WHITE, this.pixels.find { it.x == 1 && it.y == 1 }!!.value.toPixelColor())
        }
        layerOne.flattenOnto(layerThree).apply {
            Assert.assertEquals(PixelColor.WHITE, this.pixels.find { it.x == 0 && it.y == 0 }!!.value.toPixelColor())
            Assert.assertEquals(PixelColor.BLACK, this.pixels.find { it.x == 1 && it.y == 0 }!!.value.toPixelColor())
            Assert.assertEquals(PixelColor.BLACK, this.pixels.find { it.x == 0 && it.y == 1 }!!.value.toPixelColor())
            Assert.assertEquals(PixelColor.BLACK, this.pixels.find { it.x == 1 && it.y == 1 }!!.value.toPixelColor())
        }
    }

    @Test
    fun testImageFlattening() {
        val image = decodeIntoImage(sequenceOf(0, 2, 2, 2, 1, 1, 2, 2, 2, 2, 1, 2, 0, 0, 0, 0), Size(2, 2))
        image.flatten().apply {
            Assert.assertEquals(1, this.layers.count())
            this.layers[0].apply {
                Assert.assertEquals(PixelColor.BLACK, this.pixels.find { it.x == 0 && it.y == 0 }!!.value.toPixelColor())
                Assert.assertEquals(PixelColor.WHITE, this.pixels.find { it.x == 1 && it.y == 0 }!!.value.toPixelColor())
                Assert.assertEquals(PixelColor.WHITE, this.pixels.find { it.x == 0 && it.y == 1 }!!.value.toPixelColor())
                Assert.assertEquals(PixelColor.BLACK, this.pixels.find { it.x == 1 && it.y == 1 }!!.value.toPixelColor())
            }
        }
    }

    @Test
    fun testOutput() {
        val image = decodeIntoImage(sequenceOf(0, 2, 2, 2, 1, 1, 2, 2, 2, 2, 1, 2, 0, 0, 0, 0), Size(2, 2))
        val list = mutableListOf<Char>()
        flattenAndDisplayImage(image, '\n') { list += it }
        Assert.assertEquals(6, list.count())
        val string = list.map { it.toString() }.reduce { acc, s -> acc + s }
        Assert.assertEquals(" \u2588\n\u2588 \n", string)
    }
}
