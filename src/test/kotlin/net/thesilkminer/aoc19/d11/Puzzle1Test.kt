package net.thesilkminer.aoc19.d11

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test
    fun testPanelColorConversion() {
        Assert.assertEquals(PanelColor.BLACK, 0L.toPanelColor())
        Assert.assertEquals(PanelColor.WHITE, 1L.toPanelColor())
    }

    @Test
    fun testTurningDirection() {
        Assert.assertEquals(Direction.EAST, Direction.NORTH.turn(TurnDirection.RIGHT))
        Assert.assertEquals(Direction.WEST, Direction.NORTH.turn(TurnDirection.LEFT))
        Assert.assertEquals(Direction.SOUTH, Direction.SOUTH.turn(TurnDirection.LEFT).turn(TurnDirection.LEFT).turn(TurnDirection.LEFT).turn(TurnDirection.LEFT))
        Assert.assertEquals(Direction.WEST, Direction.NORTH.turn(TurnDirection.RIGHT).turn(TurnDirection.RIGHT).turn(TurnDirection.RIGHT))
    }

    @Test
    fun testTurnDirectionConversion() {
        Assert.assertEquals(TurnDirection.LEFT, 0L.toTurnDirection())
        Assert.assertEquals(TurnDirection.RIGHT, 1L.toTurnDirection())
    }

    @Test
    fun testHullPanelGetting() {
        val hull = Hull()
        val panel = hull[0, 0]
        Assert.assertEquals(panel, hull[0, 0])
        Assert.assertEquals(1, hull[1, 0].x)
        Assert.assertEquals(PanelColor.BLACK, hull[400, 300].color)
    }

    @Test
    fun testPaintedPanels() {
        val hull = Hull()
        hull[0, 0].color = PanelColor.BLACK
        hull[1, 0].color = PanelColor.WHITE
        hull[1, 0].color = PanelColor.BLACK
        hull[4, 2]
        hull[4, 3].color = PanelColor.WHITE
        Assert.assertEquals(3, findPaintedPanelsAmount(hull))
    }
}
