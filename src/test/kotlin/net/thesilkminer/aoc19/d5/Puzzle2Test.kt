package net.thesilkminer.aoc19.d5

import org.junit.Assert
import org.junit.Test

class Puzzle2Test {
    @Test(expected = UnknownOpcodeException::class)
    fun testUnknownOpCodeException() {
        IntCodeComputer.executeProgram(listOf(44, 99))
    }

    @Test
    fun testHaltNotPropagating() {
        IntCodeComputer.executeProgram(listOf(99))
        Assert.assertTrue(true)
    }

    @Test
    fun testOldIntCodePrograms() {
        IntCodeComputer.executeProgram(listOf(1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50)).apply {
            Assert.assertEquals(3_500, this[0])
        }
        IntCodeComputer.executeProgram(listOf(1, 0, 0, 0, 99)).apply {
            Assert.assertEquals(2, this[0])
        }
        IntCodeComputer.executeProgram(listOf(2, 3, 0, 3, 99)).apply {
            Assert.assertEquals(6, this[3])
        }
        IntCodeComputer.executeProgram(listOf(2, 4, 4, 5, 99, 0)).apply {
            Assert.assertEquals(9_801, this[5])
        }
        IntCodeComputer.executeProgram(listOf(1, 1, 1, 4, 99, 5, 6, 0, 99)).apply {
            Assert.assertEquals(2, this[4])
            Assert.assertEquals(30, this[0])
        }
    }

    @Test
    fun testPositionalParametersIntCodePrograms() {
        IntCodeComputer.executeProgram(listOf(1002, 4, 3, 4, 33)).apply {
            Assert.assertEquals(99, this[4])
        }
        IntCodeComputer.executeProgram(listOf(1101, 100, -1, 4, 0)).apply {
            Assert.assertEquals(99, this[4])
        }
    }

    @Test
    fun testInputOutputIntCodePrograms() {
        val list = mutableListOf<Any>()
        IntCodeComputer.executeProgram(listOf(3, 0, 4, 0, 99), listOf(44)) { list += it }
        Assert.assertTrue(list.count() == 1)
        Assert.assertEquals(44, list[0])
    }

    @Test
    fun testStringToIntCodeProgramConversion() {
        IntCodeComputer.executeProgram("1,0,0,0,99").apply {
            Assert.assertEquals(2, this[0])
        }
    }

    @Test
    fun testConditionalJumpsInstructionsIntCodeProgram() {
        val list = mutableListOf<Any>()
        IntCodeComputerPuzzle2.executeProgram(listOf(3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8), listOf(7)) { list += it }
        IntCodeComputerPuzzle2.executeProgram(listOf(3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8), listOf(8)) { list += it }
        IntCodeComputerPuzzle2.executeProgram(listOf(3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8), listOf(3)) { list += it }
        IntCodeComputerPuzzle2.executeProgram(listOf(3, 3, 1108, -1, 8, 3, 4, 3, 99), listOf(8)) { list += it }
        IntCodeComputerPuzzle2.executeProgram(listOf(3, 3, 1107, -1, 8, 3, 4, 3, 99), listOf(45)) { list += it }
        IntCodeComputerPuzzle2.executeProgram(listOf(3, 3, 1107, -1, 8, 3, 4, 3, 99), listOf(4)) { list += it }
        IntCodeComputerPuzzle2.executeProgram(listOf(3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9), listOf(44)) { list += it }
        IntCodeComputerPuzzle2.executeProgram(listOf(3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9), listOf(0)) { list += it }

        Assert.assertEquals(8, list.count())
        Assert.assertEquals(0, list[0])
        Assert.assertEquals(1, list[1])
        Assert.assertEquals(1, list[2])
        Assert.assertEquals(1, list[3])
        Assert.assertEquals(0, list[4])
        Assert.assertEquals(1, list[5])
        Assert.assertEquals(1, list[6])
        Assert.assertEquals(0, list[7])
    }

    @Test
    fun testThreeValuedJumpProgramIntCodeProgram() {
        val list = mutableListOf<Any>()

        val string = """
            3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99
        """.trimIndent()
        IntCodeComputerPuzzle2.executeProgram(string.replace("\n", "").replace("\r", ""), listOf(7)) { list += it }
        IntCodeComputerPuzzle2.executeProgram(string.replace("\n", "").replace("\r", ""), listOf(8)) { list += it }
        IntCodeComputerPuzzle2.executeProgram(string.replace("\n", "").replace("\r", ""), listOf(9)) { list += it }

        Assert.assertEquals(3, list.count())
        Assert.assertEquals(999, list[0])
        Assert.assertEquals(1_000, list[1])
        Assert.assertEquals(1_001, list[2])
    }
}
