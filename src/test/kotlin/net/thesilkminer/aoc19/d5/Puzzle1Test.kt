package net.thesilkminer.aoc19.d5

import org.junit.Assert
import org.junit.Test

class Puzzle1Test {
    @Test(expected = UnknownOpcodeException::class)
    fun testUnknownOpCodeException() {
        IntCodeComputer.executeProgram(listOf(44, 99))
    }

    @Test
    fun testHaltNotPropagating() {
        IntCodeComputer.executeProgram(listOf(99))
        Assert.assertTrue(true)
    }

    @Test
    fun testOldIntCodePrograms() {
        IntCodeComputer.executeProgram(listOf(1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50)).apply {
            Assert.assertEquals(3_500, this[0])
        }
        IntCodeComputer.executeProgram(listOf(1, 0, 0, 0, 99)).apply {
            Assert.assertEquals(2, this[0])
        }
        IntCodeComputer.executeProgram(listOf(2, 3, 0, 3, 99)).apply {
            Assert.assertEquals(6, this[3])
        }
        IntCodeComputer.executeProgram(listOf(2, 4, 4, 5, 99, 0)).apply {
            Assert.assertEquals(9_801, this[5])
        }
        IntCodeComputer.executeProgram(listOf(1, 1, 1, 4, 99, 5, 6, 0, 99)).apply {
            Assert.assertEquals(2, this[4])
            Assert.assertEquals(30, this[0])
        }
    }

    @Test
    fun testPositionalParametersIntCodePrograms() {
        IntCodeComputer.executeProgram(listOf(1002, 4, 3, 4, 33)).apply {
            Assert.assertEquals(99, this[4])
        }
        IntCodeComputer.executeProgram(listOf(1101, 100, -1, 4, 0)).apply {
            Assert.assertEquals(99, this[4])
        }
    }

    @Test
    fun testInputOutputIntCodePrograms() {
        val list = mutableListOf<Any>()
        IntCodeComputer.executeProgram(listOf(3, 0, 4, 0, 99), listOf(44)) { list += it }
        Assert.assertTrue(list.count() == 1)
        Assert.assertEquals(44, list[0])
    }

    @Test
    fun testStringToIntCodeProgramConversion() {
        IntCodeComputer.executeProgram("1,0,0,0,99").apply {
            Assert.assertEquals(2, this[0])
        }
    }
}
