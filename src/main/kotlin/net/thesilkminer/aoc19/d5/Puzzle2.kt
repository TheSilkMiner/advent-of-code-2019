package net.thesilkminer.aoc19.d5

import net.thesilkminer.aoc19.util.FileReader
import kotlin.random.Random

/*
 * --- Part Two ---
 *
 * The air conditioner comes online! Its cold air feels good for a while, but then the TEST alarms start to go off.
 * Since the air conditioner can't vent its heat anywhere but back into the spacecraft, it's actually making the air
 * inside the ship warmer.
 *
 * Instead, you'll need to use the TEST to extend the thermal radiators. Fortunately, the diagnostic program (your
 * puzzle input) is already equipped for this. Unfortunately, your Intcode computer is not.
 *
 * Your computer is only missing a few opcodes:
 *
 *     Opcode 5 is jump-if-true: if the first parameter is non-zero, it sets the instruction pointer to the value from
 *     the second parameter. Otherwise, it does nothing.
 *     Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction pointer to the value from the
 *     second parameter. Otherwise, it does nothing.
 *     Opcode 7 is less than: if the first parameter is less than the second parameter, it stores 1 in the position
 *     given by the third parameter. Otherwise, it stores 0.
 *     Opcode 8 is equals: if the first parameter is equal to the second parameter, it stores 1 in the position given by
 *     the third parameter. Otherwise, it stores 0.
 *
 * Like all instructions, these instructions need to support parameter modes as described above.
 *
 * Normally, after an instruction is finished, the instruction pointer increases by the number of values in that
 * instruction. However, if the instruction modifies the instruction pointer, that value is used and the instruction
 * pointer is not automatically increased.
 *
 * For example, here are several programs that take one input, compare it to the value 8, and then produce one output:
 *
 *     3,9,8,9,10,9,4,9,99,-1,8 - Using position mode, consider whether the input is equal to 8; output 1 (if it is) or
 *     0 (if it is not).
 *     3,9,7,9,10,9,4,9,99,-1,8 - Using position mode, consider whether the input is less than 8; output 1 (if it is) or
 *     0 (if it is not).
 *     3,3,1108,-1,8,3,4,3,99 - Using immediate mode, consider whether the input is equal to 8; output 1 (if it is) or 0
 *     (if it is not).
 *     3,3,1107,-1,8,3,4,3,99 - Using immediate mode, consider whether the input is less than 8; output 1 (if it is) or
 *     0 (if it is not).
 *
 * Here are some jump tests that take an input, then output 0 if the input was zero or 1 if the input was non-zero:
 *
 *     3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9 (using position mode)
 *     3,3,1105,-1,9,1101,0,0,12,4,12,99,1 (using immediate mode)
 *
 * Here's a larger example:
 *
 * 3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
 * 1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
 * 999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99
 *
 * The above example program uses an input instruction to ask for a single number. The program will then output 999 if
 * the input value is below 8, output 1000 if the input value is equal to 8, or output 1001 if the input value is
 * greater than 8.
 *
 * This time, when the TEST diagnostic program runs its input instruction to get the ID of the system to test, provide
 * it 5, the ID for the ship's thermal radiator controller. This diagnostic test suite only outputs one number, the
 * diagnostic code.
 *
 * What is the diagnostic code for system ID 5?
 */

// This is very ugly, I am sorry to anybody who is reading this
// My promise is that the entirety of IntCode will be ported to its own interpreter
// And I don't like this insanity too: I'll basically rename everything
typealias InputBufferPuzzle2 = Iterator<Int>

class HaltExceptionPuzzle2 : Exception("The program halted")
class UnknownOpcodeExceptionPuzzle2(opcode: Int) : Exception("The given opcode $opcode is not recognized by an IntCode CPU")
class UnknownParameterTypeExceptionPuzzle2(parameterType: Int) : Exception("The given parameter type $parameterType is not recognized by an IntCode CPU")

class InfiniteListPuzzle2<E : Any>(private val default: E, private val wrappedList: MutableList<E> = mutableListOf()) : MutableList<E> by wrappedList {
    override fun get(index: Int) = try { this.wrappedList[index] } catch (e: Exception) { this.default }
}

enum class ParameterTypePuzzle2(val mnemonic: Int, val resolve: (Int, MemoryPuzzle2) -> Int) {
    POS(0, { a, b -> b[a] }),
    IMM(1, { a, _ -> a })
}

enum class OpcodePuzzle2(val opcode: Int, val argumentAmount: Int, val operation: (List<ParameterPuzzle2>, GlobalStatePuzzle2) -> Unit) {
    SUM(1, 3, { p, gs -> gs.memory[p[2].ensureType(ParameterTypePuzzle2.POS).value] = p[0].resolve(gs) + p[1].resolve(gs) }),
    MUL(2, 3, { p, gs -> gs.memory[p[2].ensureType(ParameterTypePuzzle2.POS).value] = p[0].resolve(gs) * p[1].resolve(gs) }),
    REA(3, 1, { p, gs -> gs.memory[p[0].ensureType(ParameterTypePuzzle2.POS).value] = gs.inputs.next() }),
    WRT(4, 1, { p, gs -> gs.outputStream(p[0].resolve(gs)) }),
    JMT(5, 2, { p, gs -> if (p[0].resolve(gs) != 0) gs.insnPointer = p[1].resolve(gs) else Unit }),
    JMF(6, 2, { p, gs -> if (p[0].resolve(gs) == 0) gs.insnPointer = p[1].resolve(gs) else Unit }),
    LTH(7, 3, { p, gs -> gs.memory[p[2].ensureType(ParameterTypePuzzle2.POS).value] = if (p[0].resolve(gs) < p[1].resolve(gs)) 1 else 0 }),
    EQL(8, 3, { p, gs -> gs.memory[p[2].ensureType(ParameterTypePuzzle2.POS).value] = if (p[0].resolve(gs) == p[1].resolve(gs)) 1 else 0 }),
    HAL(99, 0, { _, _ -> throw HaltExceptionPuzzle2() })
}

data class ParameterPuzzle2(val value: Int, val ParameterTypePuzzle2: ParameterTypePuzzle2) {
    fun ensureType(type: ParameterTypePuzzle2) = this.apply {
        check(this.ParameterTypePuzzle2 == type) { "Mismatching type found: expected $type, found ${this.ParameterTypePuzzle2}" }
    }
    fun resolve(globalState: GlobalStatePuzzle2) = this.resolve(globalState.memory)

    private fun resolve(memory: MemoryPuzzle2) = this.ParameterTypePuzzle2.resolve(this.value, memory)
}

class MemoryPuzzle2(private val size: Int) {
    private val contents = IntArray(this.size)

    val dump: String get() = this.contents.toList().toString()

    init {
        this.clear()
    }

    operator fun get(index: Int): Int {
        require(index in 0 until this.size) { "Index $index is outside the bounds of memory 0..${this.size}" }
        return this.contents[index]
    }
    operator fun set(index: Int, value: Int) {
        require(index in 0 until this.size) { "Index $index is outside the bounds of memory 0..${this.size}" }
        this.contents[index] = value
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun fill(from: Int, vararg values: Int) {
        require(from in 0 until this.size) { "Index $from is outside the bounds of memory 0..${this.size}" }
        require(from + values.count() - 1 in 0 until this.size) { "Unable to fill areas outside memory" }
        for (i in 0 until values.count()) this[from + i] = values[i]
    }
    fun fill(from: Int, values: List<Int>) = fill(from, *values.toTypedArray().toIntArray())

    @Suppress("MemberVisibilityCanBePrivate")
    fun clear() = (0 until this.size).forEach { this.contents[it] = Random.Default.nextInt() }
}

class CpuPuzzle2 {
    var isBusy = false

    private fun runInstruction(globalState: GlobalStatePuzzle2) {
        val (opcode, paramTypeList) = globalState.memory[globalState.insnPointer++].split()
        val rawParameters = opcode.gatherArgumentsIntoList(globalState)
        val parameters = paramTypeList.matchTo(rawParameters)
        try {
            opcode.operation(parameters, globalState)
        } catch (e: HaltExceptionPuzzle2) {
            throw e
        } catch (e: Exception) {
            throw Exception("Unable to execute instruction $opcode $parameters (currently pointing at ${globalState.insnPointer})\nMemory dump: ${globalState.memory.dump}", e)
        }
    }

    fun submit(globalState: GlobalStatePuzzle2) {
        try {
            this.isBusy = true
            while (true) {
                this.runInstruction(globalState)
            }
        } catch (e: HaltExceptionPuzzle2) {
            // Program halted
        } finally {
            globalState.insnPointer = 0
            this.isBusy = false
        }
    }

    private fun OpcodePuzzle2.gatherArgumentsIntoList(gs: GlobalStatePuzzle2) = (0 until this.argumentAmount).map { gs.memory[gs.insnPointer++] }
    private fun InfiniteListPuzzle2<ParameterTypePuzzle2>.matchTo(rawParameters: List<Int>) = rawParameters.mapIndexed { i, p -> ParameterPuzzle2(p, this[i]) }

    private fun Int.split() = Pair((this % 100).toOpcodePuzzle2(), this.toParameterTypePuzzle2List())
    private fun Int.toOpcodePuzzle2() = OpcodePuzzle2.values().find { it.opcode == this % 100 } ?: throw UnknownOpcodeExceptionPuzzle2(this)
    private fun Int.toParameterTypePuzzle2() = ParameterTypePuzzle2.values().find { it.mnemonic == this % 10 }
        ?: throw UnknownParameterTypeExceptionPuzzle2(this)

    private fun Int.toParameterTypePuzzle2List(): InfiniteListPuzzle2<ParameterTypePuzzle2> {
        val list = InfiniteListPuzzle2(ParameterTypePuzzle2.POS)
        var paramType = this / 100
        while (paramType != 0) {
            list += paramType.toParameterTypePuzzle2()
            paramType /= 10
        }
        return list
    }
}

@Suppress("SpellCheckingInspection")
data class GlobalStatePuzzle2(val memory: MemoryPuzzle2, var insnPointer: Int, val inputs: InputBufferPuzzle2, val outputStream: (Any) -> Unit)

object IntCodeComputerPuzzle2 {
    private val cpuList = listOf(CpuPuzzle2())

    fun executeProgram(instructions: List<Int>, inputQueue: List<Int> = listOf(), outputStream: (Any) -> Unit = ::println): MemoryPuzzle2 {
        val globalState = GlobalStatePuzzle2(MemoryPuzzle2(instructions.count()).apply { this.fill(0, instructions) }, 0, inputQueue.iterator(), outputStream)
        this.cpuList.find { !it.isBusy }?.submit(globalState) ?: throw IllegalStateException("No not busy CPU found")
        return globalState.memory
    }

    fun executeProgram(instructions: String, inputQueue: List<Int> = listOf(), outputStream: (Any) -> Unit = ::println) =
        this.executeProgram(instructions.split(",").map(String::toInt), inputQueue, outputStream)
}

fun main() {
    // 9_436_229
    val instructions = FileReader("d5_p2.txt").read()[0].split(",").map(String::toInt).toMutableList()
    IntCodeComputerPuzzle2.executeProgram(instructions, listOf(5))
}
