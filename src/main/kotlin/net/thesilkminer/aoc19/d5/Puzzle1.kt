package net.thesilkminer.aoc19.d5

import net.thesilkminer.aoc19.util.FileReader
import kotlin.random.Random

/*
 * --- Day 5: Sunny with a Chance of Asteroids ---
 *
 * You're starting to sweat as the ship makes its way toward Mercury. The Elves suggest that you get the air conditioner
 * working by upgrading your ship computer to support the Thermal Environment Supervision Terminal.
 *
 * The Thermal Environment Supervision Terminal (TEST) starts by running a diagnostic program (your puzzle input). The
 * TEST diagnostic program will run on your existing Intcode computer after a few modifications:
 *
 * First, you'll need to add two new instructions:
 *
 *     Opcode 3 takes a single integer as input and saves it to the position given by its only parameter. For example,
 *     the instruction 3,50 would take an input value and store it at address 50.
 *     Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at
 *     address 50.
 *
 * Programs that use these instructions will come with documentation that explains what should be connected to the input
 * and output. The program 3,0,4,0,99 outputs whatever it gets as input, then halts.
 *
 * Second, you'll need to add support for parameter modes:
 *
 * Each parameter of an instruction is handled based on its parameter mode. Right now, your ship computer already
 * understands parameter mode 0, position mode, which causes the parameter to be interpreted as a position - if the
 * parameter is 50, its value is the value stored at address 50 in memory. Until now, all parameters have been in
 * position mode.
 *
 * Now, your ship computer will also need to handle parameters in mode 1, immediate mode. In immediate mode, a parameter
 * is interpreted as a value - if the parameter is 50, its value is simply 50.
 *
 * Parameter modes are stored in the same value as the instruction's opcode. The opcode is a two-digit number based only
 * on the ones and tens digit of the value, that is, the opcode is the rightmost two digits of the first value in an
 * instruction. Parameter modes are single digits, one per parameter, read right-to-left from the opcode: the first
 * parameter's mode is in the hundreds digit, the second parameter's mode is in the thousands digit, the third
 * parameter's mode is in the ten-thousands digit, and so on. Any missing modes are 0.
 *
 * For example, consider the program 1002,4,3,4,33.
 *
 * The first instruction, 1002,4,3,4, is a multiply instruction - the rightmost two digits of the first value, 02,
 * indicate opcode 2, multiplication. Then, going right to left, the parameter modes are 0 (hundreds digit), 1
 * (thousands digit), and 0 (ten-thousands digit, not present and therefore zero):
 *
 * ABCDE
 *  1002
 *
 * DE - two-digit opcode,      02 == opcode 2
 *  C - mode of 1st parameter,  0 == position mode
 *  B - mode of 2nd parameter,  1 == immediate mode
 *  A - mode of 3rd parameter,  0 == position mode,
 *                                   omitted due to being a leading zero
 *
 * This instruction multiplies its first two parameters. The first parameter, 4 in position mode, works like it did
 * before - its value is the value stored at address 4 (33). The second parameter, 3 in immediate mode, simply has value
 * 3. The result of this operation, 33 * 3 = 99, is written according to the third parameter, 4 in position mode, which
 * also works like it did before - 99 is written to address 4.
 *
 * Parameters that an instruction writes to will never be in immediate mode.
 *
 * Finally, some notes:
 *
 *     It is important to remember that the instruction pointer should increase by the number of values in the
 *     instruction after the instruction finishes. Because of the new instructions, this amount is no longer always 4.
 *     Integers can be negative: 1101,100,-1,4,0 is a valid program (find 100 + -1, store the result in position 4).
 *
 * The TEST diagnostic program will start by requesting from the user the ID of the system to test by running an input
 * instruction - provide it 1, the ID for the ship's air conditioner unit.
 *
 * It will then perform a series of diagnostic tests confirming that various parts of the Intcode computer, like
 * parameter modes, function correctly. For each test, it will run an output instruction indicating how far the result
 * of the test was from the expected value, where 0 means the test was successful. Non-zero outputs mean that a function
 * is not working correctly; check the instructions that were run before the output instruction to see which one failed.
 *
 * Finally, the program will output a diagnostic code and immediately halt. This final output isn't an error; an output
 * followed immediately by a halt means the program finished. If all outputs were zero except the diagnostic code, the
 * diagnostic program ran successfully.
 *
 * After providing 1 to the only input instruction and passing all the tests, what diagnostic code does the program produce?
 */
typealias InputBuffer = Iterator<Int>

class HaltException : Exception("The program halted")
class UnknownOpcodeException(opcode: Int) : Exception("The given opcode $opcode is not recognized by an IntCode CPU")
class UnknownParameterTypeException(parameterType: Int) : Exception("The given parameter type $parameterType is not recognized by an IntCode CPU")

class InfiniteList<E : Any>(private val default: E, private val wrappedList: MutableList<E> = mutableListOf()) : MutableList<E> by wrappedList {
    override fun get(index: Int) = try { this.wrappedList[index] } catch (e: Exception) { this.default }
}

enum class ParameterType(val mnemonic: Int, val resolve: (Int, Memory) -> Int) {
    POS(0, { a, b -> b[a] }),
    IMM(1, { a, _ -> a })
}

enum class Opcode(val opcode: Int, val argumentAmount: Int, val operation: (List<Parameter>, GlobalState) -> Unit) {
    SUM(1, 3, { p, gs -> gs.memory[p[2].ensureType(ParameterType.POS).value] = p[0].resolve(gs) + p[1].resolve(gs) }),
    MUL(2, 3, { p, gs -> gs.memory[p[2].ensureType(ParameterType.POS).value] = p[0].resolve(gs) * p[1].resolve(gs) }),
    REA(3, 1, { p, gs -> gs.memory[p[0].ensureType(ParameterType.POS).value] = gs.inputs.next() }),
    WRT(4, 1, { p, gs -> gs.outputStream(p[0].resolve(gs)) }),
    HAL(99, 0, { _, _ -> throw HaltException() })
}

data class Parameter(val value: Int, val parameterType: ParameterType) {
    fun ensureType(type: ParameterType) = this.apply { check(this.parameterType == type) { "Mismatching type found: expected $type, found ${this.parameterType}" } }
    fun resolve(globalState: GlobalState) = this.resolve(globalState.memory)

    private fun resolve(memory: Memory) = this.parameterType.resolve(this.value, memory)
}

class Memory(private val size: Int) {
    private val contents = IntArray(this.size)

    val dump: String get() = this.contents.toList().toString()

    init {
        this.clear()
    }

    operator fun get(index: Int): Int {
        require(index in 0 until this.size) { "Index $index is outside the bounds of memory 0..${this.size}" }
        return this.contents[index]
    }
    operator fun set(index: Int, value: Int) {
        require(index in 0 until this.size) { "Index $index is outside the bounds of memory 0..${this.size}" }
        this.contents[index] = value
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun fill(from: Int, vararg values: Int) {
        require(from in 0 until this.size) { "Index $from is outside the bounds of memory 0..${this.size}" }
        require(from + values.count() - 1 in 0 until this.size) { "Unable to fill areas outside memory" }
        for (i in 0 until values.count()) this[from + i] = values[i]
    }
    fun fill(from: Int, values: List<Int>) = fill(from, *values.toTypedArray().toIntArray())

    @Suppress("MemberVisibilityCanBePrivate")
    fun clear() = (0 until this.size).forEach { this.contents[it] = Random.Default.nextInt() }
}

class Cpu {
    var isBusy = false

    @Suppress("SpellCheckingInspection") private var insnPointer = 0

    private fun runInstruction(globalState: GlobalState) {
        val (opcode, paramTypeList) = globalState.memory[this.insnPointer++].split()
        val rawParameters = opcode.gatherArgumentsIntoList(globalState.memory)
        val parameters = paramTypeList.matchTo(rawParameters)
        try {
            opcode.operation(parameters, globalState)
        } catch (e: HaltException) {
            throw e
        } catch (e: Exception) {
            throw Exception("Unable to execute instruction $opcode $parameters (currently pointing at ${this.insnPointer})\nMemory dump: ${globalState.memory.dump}", e)
        }
    }

    fun submit(globalState: GlobalState) {
        try {
            this.isBusy = true
            while (true) {
                this.runInstruction(globalState)
            }
        } catch (e: HaltException) {
            // Program halted
        } finally {
            this.insnPointer = 0
            this.isBusy = false
        }
    }

    private fun Opcode.gatherArgumentsIntoList(memory: Memory) = (0 until this.argumentAmount).map { memory[this@Cpu.insnPointer++] }
    private fun InfiniteList<ParameterType>.matchTo(rawParameters: List<Int>) = rawParameters.mapIndexed { i, p -> Parameter(p, this[i]) }

    private fun Int.split() = Pair((this % 100).toOpcode(), this.toParameterTypeList())
    private fun Int.toOpcode() = Opcode.values().find { it.opcode == this % 100 } ?: throw UnknownOpcodeException(this)
    private fun Int.toParameterType() = ParameterType.values().find { it.mnemonic == this % 10 } ?: throw UnknownParameterTypeException(this)

    private fun Int.toParameterTypeList(): InfiniteList<ParameterType> {
        val list = InfiniteList(ParameterType.POS)
        var paramType = this / 100
        while (paramType != 0) {
            list += paramType.toParameterType()
            paramType /= 10
        }
        return list
    }
}

data class GlobalState(val memory: Memory, val inputs: InputBuffer, val outputStream: (Any) -> Unit)

object IntCodeComputer {
    private val cpuList = listOf(Cpu())

    fun executeProgram(instructions: List<Int>, inputQueue: List<Int> = listOf(), outputStream: (Any) -> Unit = ::println): Memory {
        val globalState = GlobalState(Memory(instructions.count()).apply { this.fill(0, instructions) }, inputQueue.iterator(), outputStream)
        this.cpuList.find { !it.isBusy }?.submit(globalState) ?: throw IllegalStateException("No not busy CPU found")
        return globalState.memory
    }

    fun executeProgram(instructions: String, inputQueue: List<Int> = listOf(), outputStream: (Any) -> Unit = ::println) =
        this.executeProgram(instructions.split(",").map(String::toInt), inputQueue, outputStream)
}

fun main() {
    // 2_845_163
    val instructions = FileReader("d5_p1.txt").read()[0].split(",").map(String::toInt).toMutableList()
    IntCodeComputer.executeProgram(instructions, listOf(1))
}

