package net.thesilkminer.aoc19.d9

import net.thesilkminer.aoc19.util.FileReader
import java.util.concurrent.Callable
import java.util.concurrent.ExecutionException
import java.util.concurrent.Executors
import java.util.concurrent.Future

/*
 * --- Day 9: Sensor Boost ---
 *
 * You've just said goodbye to the rebooted rover and left Mars when you receive a faint distress signal coming from the
 * asteroid belt. It must be the Ceres monitoring station!
 *
 * In order to lock on to the signal, you'll need to boost your sensors. The Elves send up the latest BOOST program -
 * Basic Operation Of System Test.
 *
 * While BOOST (your puzzle input) is capable of boosting your sensors, for tenuous safety reasons, it refuses to do so
 * until the computer it runs on passes some checks to demonstrate it is a complete Intcode computer.
 *
 * Your existing Intcode computer is missing one key feature: it needs support for parameters in relative mode.
 *
 * Parameters in mode 2, relative mode, behave very similarly to parameters in position mode: the parameter is
 * interpreted as a position. Like position mode, parameters in relative mode can be read from or written to.
 *
 * The important difference is that relative mode parameters don't count from address 0. Instead, they count from a
 * value called the relative base. The relative base starts at 0.
 *
 * The address a relative mode parameter refers to is itself plus the current relative base. When the relative base is
 * 0, relative mode parameters and position mode parameters with the same value refer to the same address.
 *
 * For example, given a relative base of 50, a relative mode parameter of -7 refers to memory address 50 + -7 = 43.
 *
 * The relative base is modified with the relative base offset instruction:
 *
 *     Opcode 9 adjusts the relative base by the value of its only parameter. The relative base increases (or decreases,
 *     if the value is negative) by the value of the parameter.
 *
 * For example, if the relative base is 2000, then after the instruction 109,19, the relative base would be 2019. If the
 * next instruction were 204,-34, then the value at address 1985 would be output.
 *
 * Your Intcode computer will also need a few other capabilities:
 *
 *     The computer's available memory should be much larger than the initial program. Memory beyond the initial program
 *     starts with the value 0 and can be read or written like any other memory. (It is invalid to try to access memory
 *     at a negative address, though.)
 *     The computer should have support for large numbers. Some instructions near the beginning of the BOOST program
 *     will verify this capability.
 *
 * Here are some example programs that use these features:
 *
 *     109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99 takes no input and produces a copy of itself as output.
 *     1102,34915192,34915192,7,4,7,99,0 should output a 16-digit number.
 *     104,1125899906842624,99 should output the large number in the middle.
 *
 * The BOOST program will ask for a single input; run it in test mode by providing it the value 1. It will perform a
 * series of checks on each opcode, output any opcodes (and the associated parameter modes) that seem to be functioning
 * incorrectly, and finally output a BOOST keycode.
 *
 * Once your Intcode computer is fully functional, the BOOST program should report no malfunctioning opcodes when run in
 * test mode; it should only output a single value, the BOOST keycode. What BOOST keycode does it produce?
 */
typealias InputBuffer = Iterator<Long>

class HaltException : Exception("The program halted")
class UnknownOpcodeException(opcode: Long) : Exception("The given opcode $opcode is not recognized by an IntCode CPU")
class UnknownParameterTypeException(parameterType: Long) : Exception("The given parameter type $parameterType is not recognized by an IntCode CPU")

class InfiniteList<E : Any>(private val default: E, private val wrappedList: MutableList<E> = mutableListOf()) : MutableList<E> by wrappedList {
    override fun get(index: Int) = try { this.wrappedList[index] } catch (e: Exception) { this.default }
}

enum class ParameterType(val mnemonic: Long, val resolveRead: (Long, GlobalState) -> Long, val resolveWrite: (Long, GlobalState) -> Long) {
    POS(0, { a, b -> b.memory[a] }, { a, _ -> a }),
    IMM(1, { a, _ -> a }, { _, _ -> throw UnsupportedOperationException("Unable to use an IMM parameter for writing") }),
    REL(2, { a, b -> b.memory[b.relativeBase + a] }, { a, b -> b.relativeBase + a })
}

enum class Opcode(val opcode: Long, val argumentAmount: Int, val operation: (List<Parameter>, GlobalState) -> Unit) {
    SUM(1, 3, { p, gs -> gs.memory[p[2].but(ParameterType.IMM).resolveWrite(gs)] = p[0].resolveRead(gs) + p[1].resolveRead(gs) }),
    MUL(2, 3, { p, gs -> gs.memory[p[2].but(ParameterType.IMM).resolveWrite(gs)] = p[0].resolveRead(gs) * p[1].resolveRead(gs) }),
    REA(3, 1, { p, gs -> gs.memory[p[0].but(ParameterType.IMM).resolveWrite(gs)] = gs.inputs.next() }),
    WRT(4, 1, { p, gs -> gs.outputStream(p[0].resolveRead(gs)) }),
    JMT(5, 2, { p, gs -> if (p[0].resolveRead(gs) != 0L) gs.insnPointer = p[1].resolveRead(gs) else Unit }),
    JMF(6, 2, { p, gs -> if (p[0].resolveRead(gs) == 0L) gs.insnPointer = p[1].resolveRead(gs) else Unit }),
    LTH(7, 3, { p, gs -> gs.memory[p[2].but(ParameterType.IMM).resolveWrite(gs)] = if (p[0].resolveRead(gs) < p[1].resolveRead(gs)) 1 else 0 }),
    EQL(8, 3, { p, gs -> gs.memory[p[2].but(ParameterType.IMM).resolveWrite(gs)] = if (p[0].resolveRead(gs) == p[1].resolveRead(gs)) 1 else 0 }),
    RBA(9, 1, { p, gs -> gs.relativeBase += p[0].resolveRead(gs) }),
    HAL(99, 0, { _, _ -> throw HaltException() })
}

data class Parameter(val value: Long, val parameterType: ParameterType) {
    @Suppress("unused")
    fun only(type: ParameterType) = this.apply {
        check(this.parameterType == type) { "Mismatching type found: expected $type, found ${this.parameterType}" }
    }
    fun but(type: ParameterType) = this.apply {
        check (this.parameterType != type) { "Mismatching type found: found $type, but expected something else" }
    }
    fun resolveRead(globalState: GlobalState) = this.parameterType.resolveRead(this.value, globalState)
    fun resolveWrite(globalState: GlobalState) = this.parameterType.resolveWrite(this.value, globalState)
}

class Memory(private val size: Long) {
    private val contents = mutableMapOf<Long, Long>()

    val dump: String get() = mutableListOf<Long>().apply { (0..this@Memory.contents.keys.max()!!).forEach { this += this@Memory[it] }.let { this@Memory.purge() } }.toString()

    init {
        this.clear()
    }

    operator fun get(index: Long): Long {
        require(index in 0 until this.size) { "Index $index is outside the bounds of memory 0..${this.size}" }
        return this.contents.computeIfAbsent(index) { 0 }
    }
    operator fun set(index: Long, value: Long) {
        require(index in 0 until this.size) { "Index $index is outside the bounds of memory 0..${this.size}" }
        this.contents[index] = value
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun fill(from: Long, vararg values: Long) {
        require(from in 0 until this.size) { "Index $from is outside the bounds of memory 0..${this.size}" }
        require(from + values.count() - 1 in 0 until this.size) { "Unable to fill areas outside memory" }
        for (i in 0 until values.count()) this[from + i] = values[i]
    }
    fun fill(from: Long, values: List<Long>) = fill(from, *values.toTypedArray().toLongArray())

    private fun purge() = with (this.contents) {
        val copy = mutableMapOf<Long, Long>()
        this.forEach { (k, v) -> if (v == 0L) Unit else copy[k] = v }
        this@Memory.clear()
        copy.forEach { (k, v) -> this@Memory[k] = v }
    }
    private fun clear() = this.contents.clear()
}

class Cpu {
    private fun runInstruction(globalState: GlobalState) {
        val (opcode, paramTypeList) = globalState.memory[globalState.insnPointer++].split()
        val rawParameters = opcode.gatherArgumentsIntoList(globalState)
        val parameters = paramTypeList.matchTo(rawParameters)
        try {
            opcode.operation(parameters, globalState)
        } catch (e: HaltException) {
            throw e
        } catch (e: Exception) {
            globalState.insnPointer -= opcode.argumentAmount + 1
            throw Exception("Unable to execute instruction $opcode $parameters (currently pointing at ${globalState.insnPointer} with relative base ${globalState.relativeBase})\n" +
                    "Memory dump: ${globalState.memory.dump}", e)
        }
    }

    fun submit(globalState: GlobalState) {
        try {
            while (true) {
                this.runInstruction(globalState)
            }
        } catch (e: HaltException) {
            // Program halted
            globalState.insnPointer = -1
        }
    }

    private fun Opcode.gatherArgumentsIntoList(gs: GlobalState) = (0 until this.argumentAmount).map { gs.memory[gs.insnPointer++] }
    private fun InfiniteList<ParameterType>.matchTo(rawParameters: List<Long>) = rawParameters.mapIndexed { i, p -> Parameter(p, this[i]) }

    private fun Long.split() = Pair((this % 100).toOpcode(), this.toParameterTypeList())
    private fun Long.toOpcode() = Opcode.values().find { it.opcode == this % 100 } ?: throw UnknownOpcodeException(this)
    private fun Long.toParameterType() = ParameterType.values().find { it.mnemonic == this % 10 }
        ?: throw UnknownParameterTypeException(this)

    private fun Long.toParameterTypeList(): InfiniteList<ParameterType> {
        val list = InfiniteList(ParameterType.POS)
        var paramType = this / 100
        while (paramType != 0L) {
            list += paramType.toParameterType()
            paramType /= 10
        }
        return list
    }
}

@Suppress("SpellCheckingInspection")
data class GlobalState(val memory: Memory, var relativeBase: Long, var insnPointer: Long, val inputs: InputBuffer, val outputStream: (Long) -> Unit)

object IntCodeComputer {
    private val cpuList = Executors.newFixedThreadPool(5)

    fun executeProgram(instructions: List<Long>, inputQueue: Sequence<Long> = sequenceOf(), outputStream: (Long) -> Unit = ::println): Future<GlobalState> {
        return this.cpuList.submit(Callable {
            GlobalState(Memory(Long.MAX_VALUE).apply { this.fill(0, instructions) }, 0, 0, inputQueue.iterator(), outputStream).apply {
                Cpu().submit(this)
            }
        })
    }

    fun executeSyncProgram(instructions: List<Long>, inputQueue: Sequence<Long> = sequenceOf(), outputStream: (Long) -> Unit = ::println) : GlobalState {
        return try {
            this.executeProgram(instructions, inputQueue, outputStream).get()
        } catch (e: ExecutionException) {
            throw e.cause ?: e
        }
    }

    fun stop() {
        this.cpuList.shutdown()
    }
}

fun main() {
    // 3_765_554_916
    IntCodeComputer.executeSyncProgram(FileReader("d9_p1.txt").read()[0].split(",").map(String::toLong), sequenceOf(1)).let { IntCodeComputer.stop() }
}
