package net.thesilkminer.aoc19.d12

import net.thesilkminer.aoc19.util.FileReader

/*
 * --- Part Two ---
 *
 * All this drifting around in space makes you wonder about the nature of the universe. Does history really repeat
 * itself? You're curious whether the moons will ever return to a previous state.
 *
 * Determine the number of steps that must occur before all of the moons' positions and velocities exactly match a
 * previous point in time.
 *
 * For example, the first example above takes 2772 steps before they exactly match a previous point in time; it
 * eventually returns to the initial state:
 *
 * After 0 steps:
 * pos=<x= -1, y=  0, z=  2>, vel=<x=  0, y=  0, z=  0>
 * pos=<x=  2, y=-10, z= -7>, vel=<x=  0, y=  0, z=  0>
 * pos=<x=  4, y= -8, z=  8>, vel=<x=  0, y=  0, z=  0>
 * pos=<x=  3, y=  5, z= -1>, vel=<x=  0, y=  0, z=  0>
 *
 * After 2770 steps:
 * pos=<x=  2, y= -1, z=  1>, vel=<x= -3, y=  2, z=  2>
 * pos=<x=  3, y= -7, z= -4>, vel=<x=  2, y= -5, z= -6>
 * pos=<x=  1, y= -7, z=  5>, vel=<x=  0, y= -3, z=  6>
 * pos=<x=  2, y=  2, z=  0>, vel=<x=  1, y=  6, z= -2>
 *
 * After 2771 steps:
 * pos=<x= -1, y=  0, z=  2>, vel=<x= -3, y=  1, z=  1>
 * pos=<x=  2, y=-10, z= -7>, vel=<x= -1, y= -3, z= -3>
 * pos=<x=  4, y= -8, z=  8>, vel=<x=  3, y= -1, z=  3>
 * pos=<x=  3, y=  5, z= -1>, vel=<x=  1, y=  3, z= -1>
 *
 * After 2772 steps:
 * pos=<x= -1, y=  0, z=  2>, vel=<x=  0, y=  0, z=  0>
 * pos=<x=  2, y=-10, z= -7>, vel=<x=  0, y=  0, z=  0>
 * pos=<x=  4, y= -8, z=  8>, vel=<x=  0, y=  0, z=  0>
 * pos=<x=  3, y=  5, z= -1>, vel=<x=  0, y=  0, z=  0>
 *
 * Of course, the universe might last for a very long time before repeating. Here's a copy of the second example from
 * above:
 *
 * <x=-8, y=-10, z=0>
 * <x=5, y=5, z=10>
 * <x=2, y=-7, z=3>
 * <x=9, y=-8, z=-3>
 *
 * This set of initial positions takes 4686774924 steps before it repeats a previous state! Clearly, you might need to
 * find a more efficient way to simulate the universe.
 *
 * How many steps does it take to reach the first state that exactly matches a previous state?
 */
data class AxisState(val position: Int, val velocity: Int)

fun gcd(a: Long, b: Long): Long = if (b == 0L) a else gcd(b, a % b)
fun lcm(a: Long, b: Long) = Math.multiplyExact(a, (b / gcd(a, b)))

fun checkForPreviousStates(data: List<String>): Long {
    val states = mutableMapOf<Char, MutableSet<List<AxisState>>>()
    val space = simulateSpaceForTicks(0, data)
    var counterXAxis = 0L
    var counterYAxis = 0L
    var counterZAxis = 0L
    var counter = 0L
    while (true) {
        val xStates = space.bodies.map { AxisState(it.position.x, it.velocity.x) }
        val yStates = space.bodies.map { AxisState(it.position.y, it.velocity.y) }
        val zStates = space.bodies.map { AxisState(it.position.z, it.velocity.z) }

        if (counterXAxis == 0L && !states.computeIfAbsent('x') { mutableSetOf() }.add(xStates)) {
            counterXAxis = counter
        }
        if (counterYAxis == 0L && !states.computeIfAbsent('y') { mutableSetOf() }.add(yStates)) {
            counterYAxis = counter
        }
        if (counterZAxis == 0L && !states.computeIfAbsent('z') { mutableSetOf() }.add(zStates)) {
            counterZAxis = counter
        }

        if (counterXAxis != 0L && counterYAxis != 0L && counterZAxis != 0L) break

        space.tick()
        ++counter
        //println("counter $counter")
    }
    return lcm(counterXAxis, lcm(counterYAxis, counterZAxis))
}

fun main() {
    // 295_693_702_908_636
    println(checkForPreviousStates(FileReader("d12_p2.txt").read()))
}
