package net.thesilkminer.aoc19.d10

import net.thesilkminer.aoc19.util.FileReader
import kotlin.math.pow
import kotlin.math.sqrt

/*
 * --- Day 10: Monitoring Station ---
 *
 * You fly into the asteroid belt and reach the Ceres monitoring station. The Elves here have an emergency: they're
 * having trouble tracking all of the asteroids and can't be sure they're safe.
 *
 * The Elves would like to build a new monitoring station in a nearby area of space; they hand you a map of all of the
 * asteroids in that region (your puzzle input).
 *
 * The map indicates whether each position is empty (.) or contains an asteroid (#). The asteroids are much smaller than
 * they appear on the map, and every asteroid is exactly in the center of its marked position. The asteroids can be
 * described with X,Y coordinates where X is the distance from the left edge and Y is the distance from the top edge (so
 * the top-left corner is 0,0 and the position immediately to its right is 1,0).
 *
 * Your job is to figure out which asteroid would be the best place to build a new monitoring station. A monitoring
 * station can detect any asteroid to which it has direct line of sight - that is, there cannot be another asteroid
 * exactly between them. This line of sight can be at any angle, not just lines aligned to the grid or diagonally. The
 * best location is the asteroid that can detect the largest number of other asteroids.
 *
 * For example, consider the following map:
 *
 * .#..#
 * .....
 * #####
 * ....#
 * ...##
 *
 * The best location for a new monitoring station on this map is the highlighted asteroid at 3,4 because it can detect 8
 * asteroids, more than any other location. (The only asteroid it cannot detect is the one at 1,0; its view of this
 * asteroid is blocked by the asteroid at 2,2.) All other asteroids are worse locations; they can detect 7 or fewer
 * other asteroids. Here is the number of other asteroids a monitoring station on each asteroid could detect:
 *
 * .7..7
 * .....
 * 67775
 * ....7
 * ...87
 *
 * Here is an asteroid (#) and some examples of the ways its line of sight might be blocked. If there were another
 * asteroid at the location of a capital letter, the locations marked with the corresponding lowercase letter would be
 * blocked and could not be detected:
 *
 * #.........
 * ...A......
 * ...B..a...
 * .EDCG....a
 * ..F.c.b...
 * .....c....
 * ..efd.c.gb
 * .......c..
 * ....f...c.
 * ...e..d..c
 *
 * Here are some larger examples:
 *
 *     Best is 5,8 with 33 other asteroids detected:
 *
 *     ......#.#.
 *     #..#.#....
 *     ..#######.
 *     .#.#.###..
 *     .#..#.....
 *     ..#....#.#
 *     #..#....#.
 *     .##.#..###
 *     ##...#..#.
 *     .#....####
 *
 *     Best is 1,2 with 35 other asteroids detected:
 *
 *     #.#...#.#.
 *     .###....#.
 *     .#....#...
 *     ##.#.#.#.#
 *     ....#.#.#.
 *     .##..###.#
 *     ..#...##..
 *     ..##....##
 *     ......#...
 *     .####.###.
 *
 *     Best is 6,3 with 41 other asteroids detected:
 *
 *     .#..#..###
 *     ####.###.#
 *     ....###.#.
 *     ..###.##.#
 *     ##.##.#.#.
 *     ....###..#
 *     ..#.#..#.#
 *     #..#.#.###
 *     .##...##.#
 *     .....#.#..
 *
 *     Best is 11,13 with 210 other asteroids detected:
 *
 *     .#..##.###...#######
 *     ##.############..##.
 *     .#.######.########.#
 *     .###.#######.####.#.
 *     #####.##.#.##.###.##
 *     ..#####..#.#########
 *     ####################
 *     #.####....###.#.#.##
 *     ##.#################
 *     #####.##.###..####..
 *     ..######..##.#######
 *     ####.##.####...##..#
 *     .#####..#.######.###
 *     ##...#.##########...
 *     #.##########.#######
 *     .####.#.###.###.#.##
 *     ....##.##.###..#####
 *     .#.#.###########.###
 *     #.#.#.#####.####.###
 *     ###.##.####.##.#..##
 *
 * Find the best location for a new monitoring station. How many other asteroids can be detected from that location?
 */
enum class TileType(val marker: Char) {
    ASTEROID('#'),
    EMPTY_SPACE('.')
}

fun Char.toTileType() = TileType.values().first { it.marker == this }

data class Tile(val x: Int, val y: Int, val tileType: TileType)

class Map(val xSize: Int, val ySize: Int, val tiles: List<Tile>) {
    companion object {
        fun fromMapData(strings: List<String>) : Map {
            val filteredStrings = strings.filter(String::isNotBlank)
            val ySize = filteredStrings.count()
            if (ySize == 0) return Map(0, 0, listOf())

            var xSize = -1

            val tiles = filteredStrings.asSequence()
                .onEach { if (xSize == -1) xSize = it.count() else check(it.count() == xSize) { "Map must be a rectangle" } }
                .mapIndexed { y, data -> data.asSequence().mapIndexed { x, code -> Tile(x, y, code.toTileType()) } }
                .flatMap { it }
                .toList()

            return Map(xSize, ySize, tiles)
        }
    }

    operator fun get(x: Int, y: Int) = this.tiles.find { it.x == x && it.y == y } ?: throw IndexOutOfBoundsException("$x, $y")
    operator fun get(x: Double, y: Double) = this[x.toInt(), y.toInt()]
}

fun Int.hasSameSignAs(other: Int) = this * other >= 0

fun Tile.isAsteroid() = this.tileType == TileType.ASTEROID
fun Tile.findSlopeBetween(other: Tile) = if (other.x == this.x) Double.POSITIVE_INFINITY else (other.y - this.y).toDouble() / (other.x - this.x).toDouble()
fun Tile.distanceTo(other: Tile) = sqrt((other.y.toDouble() - this.y.toDouble()).pow(2) + (other.x.toDouble() - this.x.toDouble()).pow(2))

fun Tile.canSeeHorizontal(other: Tile, directionX: Int, map: Map): Boolean {
    val delta = if (directionX < 0) -1 else 1
    var x = this.x
    while (x != other.x) {
        x += delta
        if (map[x, this.y].isAsteroid() && x != other.x) return false
    }
    return true
}

fun Tile.canSeeVertical(other: Tile, directionY: Int, map: Map): Boolean {
    val delta = if (directionY < 0) -1 else 1
    var y = this.y
    while (y != other.y) {
        y += delta
        if (map[this.x, y].isAsteroid() && y != other.y) return false
    }
    return true
}

fun Tile.canSee(other: Tile, map: Map): Boolean {
    val directionX = other.x - this.x
    val directionY = other.y - this.y
    val thisSlope = this.findSlopeBetween(other)
    if (thisSlope == 0.0) return this.canSeeHorizontal(other, directionX, map)
    if (thisSlope == Double.POSITIVE_INFINITY) return this.canSeeVertical(other, directionY, map)
    val allOtherSlopes = map.tiles.asSequence().filter { it.isAsteroid() && it != this && it != other }.map { this.findSlopeBetween(it) }.toList()
    if (!allOtherSlopes.contains(thisSlope)) return true
    val allOtherConflicting = map.tiles.filter { it.isAsteroid() && it != this && this.findSlopeBetween(it) == thisSlope }
    val nearest = allOtherConflicting.filter { (it.x - this.x).hasSameSignAs(directionX) && (it.y - this.y).hasSameSignAs(directionY) }.minBy { this.distanceTo(it) }!!
    return nearest == other
}

fun findVisibleAsteroidsFrom(map: Map, tile: Tile): List<Tile> = map.tiles.filter { it != tile && it.isAsteroid() }.filter { tile.canSee(it, map) }

fun findAllLocations(map: Map) = map.tiles.asSequence()
    .filter { it.tileType == TileType.ASTEROID }
    .map { Pair(it, findVisibleAsteroidsFrom(map, it).count()) }
    .sortedByDescending { it.second }
    .toList()

fun findBestLocation(map: Map) = findAllLocations(map).first()

fun main() {
    // 329
    println(findBestLocation(Map.fromMapData(FileReader("d10_p1.txt").read())).second)
}
