package net.thesilkminer.aoc19.d13

import net.thesilkminer.aoc19.d9.IntCodeComputer
import net.thesilkminer.aoc19.util.FileReader
import kotlin.properties.Delegates

/*
 * --- Part Two ---
 *
 * The game didn't run because you didn't put in any quarters. Unfortunately, you did not bring any quarters. Memory
 * address 0 represents the number of quarters that have been inserted; set it to 2 to play for free.
 *
 * The arcade cabinet has a joystick that can move left and right. The software reads the position of the joystick with
 * input instructions:
 *
 *     If the joystick is in the neutral position, provide 0.
 *     If the joystick is tilted to the left, provide -1.
 *     If the joystick is tilted to the right, provide 1.
 *
 * The arcade cabinet also has a segment display capable of showing a single number that represents the player's current
 * score. When three output instructions specify X=-1, Y=0, the third output instruction is not a tile; the value
 * instead specifies the new score to show in the segment display. For example, a sequence of output values like
 * -1,0,12345 would show 12345 as the player's current score.
 *
 * Beat the game by breaking all the blocks. What is your score after the last block is broken?
 */
sealed class TileType2(val mnemonic: Int, val output: String)
class EmptyTileType2 : TileType2(0, " ")
class WallTileType2 : TileType2(1, "\u2588")
class BlockTileType2 : TileType2(2, "X")
class HorizontalPaddleTileType2 : TileType2(3, "-")
class BallTileType2 : TileType2(4, "O")
class OtherTileType2(mnemonic: Int) : TileType2(mnemonic, mnemonic.toString())

fun Int.toTileType2() = when (this) {
    0 -> EmptyTileType2()
    1 -> WallTileType2()
    2 -> BlockTileType2()
    3 -> HorizontalPaddleTileType2()
    4 -> BallTileType2()
    else -> OtherTileType2(this)
}

data class Tile2(val x: Int, val y: Int, val tileType: TileType2)

class ScreenSimulator2 {
    val gpu = mutableListOf<Tile2>()

    fun draw(tile: Tile2) {
        if (this.gpu.find { it.x == tile.x && it.y == tile.y } != null) {
            this.gpu.remove(this.gpu.find { it.x == tile.x && it.y == tile.y }!!)
        }
        this.gpu += tile
    }

    fun renderTo(newLineMarker: String = "\n", stream: (String) -> Unit) {
        val maxX = this.gpu.map { it.x }.max()!!
        val minX = 0
        val maxY = this.gpu.map { it.y }.max()!!
        val minY = 0
        val glitchCharacter = "?"
        (minY..maxY).forEach { y ->
            (minX..maxX).forEach { x ->
                stream(this.gpu.find { it.x == x && it.y == y }?.tileType?.output ?: glitchCharacter)
            }
            stream(newLineMarker)
        }
        stream("Score: ")
        val score = this.gpu.find { it.x == -1 && it.y == 0 }
        if (score != null) stream(score.tileType.output) else stream("INSERT COIN")
        stream(newLineMarker)
    }
}

class Arcade2(private val game: List<Long>) {
    val screen = ScreenSimulator2()

    private val buffer = mutableListOf<Long>()
    private var render by Delegates.notNull<Boolean>()

    private fun interpretTile(long: Long) {
        this.buffer += long
        if (this.buffer.count() < 3) return
        val (x, y, type) = this.buffer
        this.buffer.clear()
        this.screen.draw(Tile2(x.toInt(), y.toInt(), type.toInt().toTileType2()))
        if (this.render) this.screen.renderTo(stream = ::print)
    }

    fun runGame(inputs: Sequence<Long> = sequenceOf(), render: Boolean = true) {
        // Using production from day 9
        this.render = render
        IntCodeComputer.executeSyncProgram(this.game, inputs, this::interpretTile).let { IntCodeComputer.stop() }
    }
}

fun simulateAndPlay(screen: ScreenSimulator2) = sequence {
    while (true) {
        val ballTile = screen.gpu.single { it.tileType is BallTileType2 }
        val paddleTile = screen.gpu.single { it.tileType is HorizontalPaddleTileType2 }
        val ballX = ballTile.x
        val paddleX = paddleTile.x
        yield(when {
            ballX < paddleX -> -1L
            ballX > paddleX -> 1L
            else -> 0L
        })
    }
}

fun main() {
    // 10_247
    val game = FileReader("d13_p2.txt").read()[0].split(",").map(String::toLong).toMutableList()
    game[0] = 2L // Let's play for free
    val arcade = Arcade2(game.toList())
    arcade.runGame(simulateAndPlay(arcade.screen), render = false)
    println(arcade.screen.gpu.find { it.x == -1 }?.tileType?.output)
}
