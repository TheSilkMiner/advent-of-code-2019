package net.thesilkminer.aoc19.d13

import net.thesilkminer.aoc19.d9.IntCodeComputer
import net.thesilkminer.aoc19.util.FileReader

/*
 * --- Day 13: Care Package ---
 *
 * As you ponder the solitude of space and the ever-increasing three-hour roundtrip for messages between you and Earth,
 * you notice that the Space Mail Indicator Light is blinking. To help keep you sane, the Elves have sent you a care
 * package.
 *
 * It's a new game for the ship's arcade cabinet! (https://en.wikipedia.org/wiki/Arcade_cabinet) Unfortunately, the
 * arcade is all the way on the other end of the ship. Surely, it won't be hard to build your own - the care package
 * even comes with schematics.
 *
 * The arcade cabinet runs Intcode software like the game the Elves sent (your puzzle input). It has a primitive screen
 * capable of drawing square tiles on a grid. The software draws tiles to the screen with output instructions: every
 * three output instructions specify the x position (distance from the left), y position (distance from the top), and
 * tile id. The tile id is interpreted as follows:
 *
 *     0 is an empty tile. No game object appears in this tile.
 *     1 is a wall tile. Walls are indestructible barriers.
 *     2 is a block tile. Blocks can be broken by the ball.
 *     3 is a horizontal paddle tile. The paddle is indestructible.
 *     4 is a ball tile. The ball moves diagonally and bounces off objects.
 *
 * For example, a sequence of output values like 1,2,3,6,5,4 would draw a horizontal paddle tile (1 tile from the left
 * and 2 tiles from the top) and a ball tile (6 tiles from the left and 5 tiles from the top).
 *
 * Start the game. How many block tiles are on the screen when the game exits?
 */
enum class TileType(val mnemonic: Int, val char: Char) {
    EMPTY(0, ' '),
    WALL(1, '\u2588'),
    BLOCK(2, 'X'),
    HORIZONTAL_PADDLE(3, '-'),
    BALL(4, 'O')
}

fun Int.toTileType() = TileType.values().find { it.mnemonic == this } ?: throw IllegalArgumentException("$this is not a valid tile type")

data class Tile(val x: Int, val y: Int, val tileType: TileType)

class ScreenSimulator {
    private val gpu = mutableListOf<Tile>()

    fun draw(tile: Tile) {
        if (this.gpu.find { it.x == tile.x && it.y == tile.y } != null) {
            this.gpu.remove(this.gpu.find { it.x == tile.x && it.y == tile.y }!!)
        }
        this.gpu += tile
    }

    fun renderTo(newLineMarker: Char = '\n', stream: (Char) -> Unit) {
        val maxX = this.gpu.map { it.x }.max()!!
        val minX = this.gpu.map { it.x }.min()!!
        val maxY = this.gpu.map { it.y }.max()!!
        val minY = this.gpu.map { it.y }.min()!!
        val glitchCharacter = '?'
        (minY..maxY).forEach { y ->
            (minX..maxX).forEach { x ->
                stream(this.gpu.find { it.x == x && it.y == y }?.tileType?.char ?: glitchCharacter)
            }
            stream(newLineMarker)
        }
    }
}

class Arcade(private val game: List<Long>) {
    private val screen = ScreenSimulator()

    private val buffer = mutableListOf<Long>()

    private fun interpretTile(long: Long) {
        this.buffer += long
        if (this.buffer.count() < 3) return
        val (x, y, type) = this.buffer
        this.buffer.clear()
        this.screen.draw(Tile(x.toInt(), y.toInt(), type.toInt().toTileType()))
    }

    fun runGame(inputs: Sequence<Long> = sequenceOf()) {
        // Using production from day 9
        IntCodeComputer.executeSyncProgram(this.game, inputs, this::interpretTile).let { IntCodeComputer.stop() }
    }

    fun renderTo(newLineMarker: Char = '\n', stream: (Char) -> Unit) = this.screen.renderTo(newLineMarker, stream)
}

fun countTileOfTypeOnScreen(game: List<Long>, tileType: TileType): Int {
    val arcade = Arcade(game)
    arcade.runGame()
    val list = mutableListOf<Char>()
    arcade.renderTo { list += it }
    return list.asSequence().filter { it == tileType.char }.count()
}

fun main() {
    // 207
    println(countTileOfTypeOnScreen(FileReader("d13_p1.txt").read()[0].split(",").map(String::toLong), TileType.BLOCK))
}
