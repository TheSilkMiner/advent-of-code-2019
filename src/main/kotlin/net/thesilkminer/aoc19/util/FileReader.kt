package net.thesilkminer.aoc19.util

import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader

class FileReader(private val path: String) {
    fun read(): List<String> {
        return BufferedReader(InputStreamReader(BufferedInputStream(this::class.java.getResourceAsStream("/" + this.path)))).use {
            return@use it.lineSequence().toList()
        }
    }
}
