package net.thesilkminer.aoc19.d8

import net.thesilkminer.aoc19.util.FileReader
import java.lang.IllegalStateException

/*
 * --- Day 8: Space Image Format ---
 *
 * The Elves' spirits are lifted when they realize you have an opportunity to reboot one of their Mars rovers, and so
 * they are curious if you would spend a brief sojourn on Mars. You land your ship near the rover.
 *
 * When you reach the rover, you discover that it's already in the process of rebooting! It's just waiting for someone
 * to enter a BIOS password. The Elf responsible for the rover takes a picture of the password (your puzzle input) and
 * sends it to you via the Digital Sending Network.
 *
 * Unfortunately, images sent via the Digital Sending Network aren't encoded with any normal encoding; instead, they're
 * encoded in a special Space Image Format. None of the Elves seem to remember why this is the case. They send you the
 * instructions to decode it.
 *
 * Images are sent as a series of digits that each represent the color of a single pixel. The digits fill each row of
 * the image left-to-right, then move downward to the next row, filling rows top-to-bottom until every pixel of the
 * image is filled.
 *
 * Each image actually consists of a series of identically-sized layers that are filled in this way. So, the first digit
 * corresponds to the top-left pixel of the first layer, the second digit corresponds to the pixel to the right of that
 * on the same layer, and so on until the last digit, which corresponds to the bottom-right pixel of the last layer.
 *
 * For example, given an image 3 pixels wide and 2 pixels tall, the image data 123456789012 corresponds to the following
 * image layers:
 *
 * Layer 1: 123
 *          456
 *
 * Layer 2: 789
 *          012
 *
 * The image you received is 25 pixels wide and 6 pixels tall.
 *
 * To make sure the image wasn't corrupted during transmission, the Elves would like you to find the layer that contains
 * the fewest 0 digits. On that layer, what is the number of 1 digits multiplied by the number of 2 digits?
 */
data class Size(val x: Int, val y: Int)
data class Pixel(val x: Int, val y: Int, val value: Int)
data class Layer(val size: Size, val pixels: List<Pixel>)
data class Image(val layers: List<Layer>)

fun decodeIntoImage(intStream: Sequence<Int>, size: Size): Image {
    operator fun MutableList<*>.unaryMinus() = this.clear()

    val layers = mutableListOf<Layer>()
    val currentPixels = mutableListOf<Pixel>()
    var x = 0
    var y = 0
    intStream.forEach {
        currentPixels += Pixel(x, y, it)
        ++x
        if (x == size.x) {
            x = 0
            ++y
        }
        if (y == size.y) {
            x = 0
            y = 0
            layers += Layer(size, currentPixels.toList()) // Copies
            -currentPixels
        }
    }

    if (x != 0 || y != 0 || currentPixels.isNotEmpty()) throw IllegalStateException("Transmission interrupted")

    return Image(layers.toList()) // Copies
}

fun Layer.findPixelValueAmount(value: Int) = this.pixels.count { it.value == value }
fun Image.findFewestPixelValuesInLayers(value: Int) = this.layers.asSequence()
    .map { Pair(it, it.findPixelValueAmount(value)) }
    .sortedBy { it.second }
    .first()
    .first

fun Image.computeChecksum() = this.findFewestPixelValuesInLayers(0).let { it.findPixelValueAmount(1) * it.findPixelValueAmount(2) }

fun main() {
    // 1_920
    println(decodeIntoImage(FileReader("d8_p1.txt").read().asSequence().flatMap { it.toCharArray().asSequence() }.map { it.toInt() - '0'.toInt() }, Size(25, 6)).computeChecksum())
}
