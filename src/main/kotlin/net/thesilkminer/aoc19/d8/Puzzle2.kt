package net.thesilkminer.aoc19.d8

import net.thesilkminer.aoc19.util.FileReader

/*
 * --- Part Two ---
 *
 * Now you're ready to decode the image. The image is rendered by stacking the layers and aligning the pixels with the
 * same positions in each layer. The digits indicate the color of the corresponding pixel: 0 is black, 1 is white, and 2
 * is transparent.
 *
 * The layers are rendered with the first layer in front and the last layer in back. So, if a given position has a
 * transparent pixel in the first and second layers, a black pixel in the third layer, and a white pixel in the fourth
 * layer, the final image would have a black pixel at that position.
 *
 * For example, given an image 2 pixels wide and 2 pixels tall, the image data 0222112222120000 corresponds to the
 * following image layers:
 *
 * Layer 1: 02
 *          22
 *
 * Layer 2: 11
 *          22
 *
 * Layer 3: 22
 *          12
 *
 * Layer 4: 00
 *          00
 *
 * Then, the full image can be found by determining the top visible pixel in each position:
 *
 *     The top-left pixel is black because the top layer is 0.
 *     The top-right pixel is white because the top layer is 2 (transparent), but the second layer is 1.
 *     The bottom-left pixel is white because the top two layers are 2, but the third layer is 1.
 *     The bottom-right pixel is black because the only visible pixel in that position is 0 (from layer 4).
 *
 * So, the final image looks like this:
 *
 * 01
 * 10
 *
 * What message is produced after decoding your image?
 */
enum class PixelColor(val mnemonic: Int, val output: () -> Char) {
    BLACK(0, { ' ' }),
    WHITE(1, { '\u2588' }),
    TRANSPARENT(2, { '?' })
}

fun Int.toPixelColor() = PixelColor.values().find { it.mnemonic == this } ?: throw IllegalArgumentException("$this is not a valid color")

fun Pixel.flattenOnto(pixel: Pixel) = when (this.value.toPixelColor()) {
    PixelColor.BLACK, PixelColor.WHITE -> this
    PixelColor.TRANSPARENT -> pixel
}

fun Layer.flattenOnto(layer: Layer): Layer {
    if (this.size != layer.size) throw IllegalArgumentException("Different sizes are not compatible when flattening (yet)")
    val newPixels = this.pixels.map { top ->
        val bottom = layer.pixels.find { it.x == top.x && it.y == top.y } ?: throw IllegalStateException("Mismatching pixel coordinates")
        top.flattenOnto(bottom)
    }
    return Layer(this.size, newPixels)
}

fun Image.flattenTop(): Image {
    if (this.layers.count() <= 1) return this
    val newFirstLayer = this.layers[0].flattenOnto(this.layers[1])
    val newLayerList = mutableListOf(newFirstLayer)
    newLayerList.addAll(this.layers.subList(fromIndex = 2, toIndex = this.layers.count()).toList()) // Copies
    return Image(newLayerList)
}

tailrec fun Image.flatten(): Image = if (this.layers.count() <= 1) this else this.flattenTop().flatten()

fun flattenAndDisplayImage(image: Image, terminator: Char, output: (Char) -> Unit) {
    val layer = image.flatten().layers.asSequence().single()
    val size = layer.size

    for (y in 0 until size.y) {
        for (x in 0 until size.x) {
            val pixel = layer.pixels.find { it.x == x && it.y == y } ?: throw IllegalStateException("Missing coordinates")
            output(pixel.value.toPixelColor().output())
        }
        output(terminator)
    }
}

fun main() {
    // PCULA
    val image = decodeIntoImage(FileReader("d8_p1.txt").read().asSequence().flatMap { it.toCharArray().asSequence() }.map { it.toInt() - '0'.toInt() }, Size(25, 6))
    flattenAndDisplayImage(image, '\n', ::print)
}
