package net.thesilkminer.aoc19.d3

import net.thesilkminer.aoc19.util.FileReader
import kotlin.math.abs

/*
 * --- Day 3: Crossed Wires ---
 *
 * The gravity assist was successful, and you're well on your way to the Venus refuelling station. During the rush back
 * on Earth, the fuel management system wasn't completely installed, so that's next on the priority list.
 *
 * Opening the front panel reveals a jumble of wires. Specifically, two wires are connected to a central port and extend
 * outward on a grid. You trace the path each wire takes as it leaves the central port, one wire per line of text (your
 * puzzle input).
 *
 * The wires twist and turn, but the two wires occasionally cross paths. To fix the circuit, you need to find the
 * intersection point closest to the central port. Because the wires are on a grid, use the Manhattan distance for this
 * measurement. While the wires do technically cross right at the central port where they both start, this point does
 * not count, nor does a wire count as crossing with itself.
 *
 * For example, if the first wire's path is R8,U5,L5,D3, then starting from the central port (o), it goes right 8, up 5,
 * left 5, and finally down 3:
 *
 * ...........
 * ...........
 * ...........
 * ....+----+.
 * ....|....|.
 * ....|....|.
 * ....|....|.
 * .........|.
 * .o-------+.
 * ...........
 *
 * Then, if the second wire's path is U7,R6,D4,L4, it goes up 7, right 6, down 4, and left 4:
 *
 * ...........
 * .+-----+...
 * .|.....|...
 * .|..+--X-+.
 * .|..|..|.|.
 * .|.-X--+.|.
 * .|..|....|.
 * .|.......|.
 * .o-------+.
 * ...........
 *
 * These wires cross at two locations (marked X), but the lower-left one is closer to the central port: its distance is 3 + 3 = 6.
 *
 * Here are a few more examples:
 *
 *     R75,D30,R83,U83,L12,D49,R71,U7,L72
 *     U62,R66,U55,R34,D71,R55,D58,R83 = distance 159
 *     R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
 *     U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = distance 135
 *
 * What is the Manhattan distance from the central port to the closest intersection?
 */
data class Point(val x: Int, val y: Int)

enum class Direction(val mnemonic: String, val xDelta: Int, val yDelta: Int) {
    NORTH("U", 0, 1),
    EAST("R", 1, 0),
    SOUTH("D", 0, -1),
    WEST("L", -1, 0)
}

data class SizedDirection(val direction: Direction, val size: Int)

class Wire(directions: List<SizedDirection>) {
    val points: MutableList<Point> = mutableListOf()

    init {
        var x = 0
        var y = 0
        directions.forEach {
            val dir = it.direction
            for (i in 0 until it.size) {
                x += dir.xDelta
                y += dir.yDelta
                points += Point(x, y)
            }
        }
    }
}

class Grid(private val wires: List<Wire>) {
    fun findIntersectionDistances() : List<Int> {
        fun Wire.findIntersectionsWith(other: Wire): Set<Point> {
            if (this == other) return setOf() // Just in case
            val set = mutableSetOf<Point>()
            this.points.forEach { if (it in other.points) set += it }
            return set
        }

        val intersectionPoints = mutableSetOf<Point>()
        this.wires.forEach { wire1 -> this.wires.forEach { wire2 -> intersectionPoints.addAll(wire1.findIntersectionsWith(wire2)) } }
        return intersectionPoints.asSequence().map { it.manhattanDistance() }.sorted().toList()
    }

    private fun Point.manhattanDistance(origin: Point = Point(0, 0)) = abs(this.x - origin.x) + abs(this.y - origin.y)
}

fun findLowestManhattanDistance(stringWires: List<String>): Int {
    fun String.toSizedDirection(): SizedDirection {
        val direction = Direction.values().first { it.mnemonic == this[0].toString() }
        val size = this.substring(startIndex = 1).toInt()
        return SizedDirection(direction, size)
    }

    val wires = stringWires.map { Wire(it.split(",").map { string -> string.toSizedDirection() }) }
    return Grid(wires).findIntersectionDistances()[0]
}

fun main() {
    // Thanks to @SizableShrimp, @borgrel and @yddGANG-er on the Together Java server
    // for suggesting me to use points rather than a gigantic grid. Your help is very
    // appreciated.
    // 2_050
    println(findLowestManhattanDistance(FileReader("d3_p1.txt").read()))
}
