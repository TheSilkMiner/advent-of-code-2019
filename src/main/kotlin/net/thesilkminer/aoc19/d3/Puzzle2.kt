package net.thesilkminer.aoc19.d3

import net.thesilkminer.aoc19.util.FileReader

/*
 * --- Part Two ---
 *
 * It turns out that this circuit is very timing-sensitive; you actually need to minimize the signal delay.
 *
 * To do this, calculate the number of steps each wire takes to reach each intersection; choose the intersection where
 * the sum of both wires' steps is lowest. If a wire visits a position on the grid multiple times, use the steps value
 * from the first time it visits that position when calculating the total value of a specific intersection.
 *
 * The number of steps a wire takes is the total number of grid squares the wire has entered to get to that location,
 * including the intersection being considered. Again consider the example from above:
 *
 * ...........
 * .+-----+...
 * .|.....|...
 * .|..+--X-+.
 * .|..|..|.|.
 * .|.-X--+.|.
 * .|..|....|.
 * .|.......|.
 * .o-------+.
 * ...........
 *
 * In the above example, the intersection closest to the central port is reached after 8+5+5+2 = 20 steps by the first
 * wire and 7+6+4+3 = 20 steps by the second wire for a total of 20+20 = 40 steps.
 *
 * However, the top-right intersection is better: the first wire takes only 8+5+2 = 15 and the second wire takes only
 * 7+6+2 = 15, a total of 15+15 = 30 steps.
 *
 * Here are the best steps for the extra examples from above:
 *
 *     R75,D30,R83,U83,L12,D49,R71,U7,L72
 *     U62,R66,U55,R34,D71,R55,D58,R83 = 610 steps
 *     R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
 *     U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = 410 steps
 *
 * What is the fewest combined steps the wires must take to reach an intersection?
 */
fun Grid.findIntersectionDistancesByStep(): List<Int> {
    fun Wire.findIntersectionsWith(other: Wire): Set<Point> {
        if (this == other) return setOf() // Just in case
        val set = mutableSetOf<Point>()
        this.points.forEach { if (it in other.points) set += it }
        return set
    }

    @Suppress("UNCHECKED_CAST")
    val wires = this::class.java.getDeclaredField("wires").apply { this.isAccessible = true }.get(this) as List<Wire>

    val intersectionPoints = mutableSetOf<Point>()
    wires.forEach { wire1 -> wires.forEach { wire2 -> intersectionPoints.addAll(wire1.findIntersectionsWith(wire2)) } }

    val map = mutableMapOf<Point, Int>()
    wires.asSequence()
        .flatMap {
            it.points
                .mapIndexed { i, point -> Pair(point, i + 1) }
                .filter { pair -> pair.first in intersectionPoints }
                .asSequence()
        }.forEach { map[it.first] = map.computeIfAbsent(it.first) { 0 } + it.second }
    return map.map { it.value }.sorted()
}

fun findLowestStepDistance(stringWires: List<String>): Int {
    fun String.toSizedDirection(): SizedDirection {
        val direction = Direction.values().first { it.mnemonic == this[0].toString() }
        val size = this.substring(startIndex = 1).toInt()
        return SizedDirection(direction, size)
    }

    val wires = stringWires.map { Wire(it.split(",").map { string -> string.toSizedDirection() }) }
    return Grid(wires).findIntersectionDistancesByStep()[0]
}

fun main() {
    // 21_666
    println(findLowestStepDistance(FileReader("d3_p2.txt").read()))
}
