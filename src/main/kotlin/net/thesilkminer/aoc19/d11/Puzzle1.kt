package net.thesilkminer.aoc19.d11

import net.thesilkminer.aoc19.d9.IntCodeComputer
import net.thesilkminer.aoc19.util.FileReader

/*
 * --- Day 11: Space Police ---
 *
 * On the way to Jupiter, you're pulled over (https://www.youtube.com/watch?v=KwY28rpyKDE) by the Space Police.
 *
 * "Attention, unmarked spacecraft! You are in violation of Space Law! All spacecraft must have a clearly visible
 * registration identifier! You have 24 hours to comply or be sent to Space Jail!"
 * (https://www.youtube.com/watch?v=BVn1oQL9sWg&t=5)
 *
 * Not wanting to be sent to Space Jail, you radio back to the Elves on Earth for help. Although it takes almost three
 * hours for their reply signal to reach you, they send instructions for how to power up the emergency hull painting
 * robot and even provide a small Intcode program (your puzzle input) that will cause it to paint your ship
 * appropriately.
 *
 * There's just one problem: you don't have an emergency hull painting robot.
 *
 * You'll need to build a new emergency hull painting robot. The robot needs to be able to move around on the grid of
 * square panels on the side of your ship, detect the color of its current panel, and paint its current panel black or
 * white. (All of the panels are currently black.)
 *
 * The Intcode program will serve as the brain of the robot. The program uses input instructions to access the robot's
 * camera: provide 0 if the robot is over a black panel or 1 if the robot is over a white panel. Then, the program will
 * output two values:
 *
 *     First, it will output a value indicating the color to paint the panel the robot is over: 0 means to paint the
 *     panel black, and 1 means to paint the panel white.
 *     Second, it will output a value indicating the direction the robot should turn: 0 means it should turn left 90
 *     degrees, and 1 means it should turn right 90 degrees.
 *
 * After the robot turns, it should always move forward exactly one panel. The robot starts facing up.
 *
 * The robot will continue running for a while like this and halt when it is finished drawing. Do not restart the
 * Intcode computer inside the robot during this process.
 *
 * For example, suppose the robot is about to start running. Drawing black panels as ., white panels as #, and the robot
 * pointing the direction it is facing (< ^ > v), the initial state and region near the robot looks like this:
 *
 * .....
 * .....
 * ..^..
 * .....
 * .....
 *
 * The panel under the robot (not visible here because a ^ is shown instead) is also black, and so any input
 * instructions at this point should be provided 0. Suppose the robot eventually outputs 1 (paint white) and then 0
 * (turn left). After taking these actions and moving forward one panel, the region now looks like this:
 *
 * .....
 * .....
 * .<#..
 * .....
 * .....
 *
 * Input instructions should still be provided 0. Next, the robot might output 0 (paint black) and then 0 (turn left):
 *
 * .....
 * .....
 * ..#..
 * .v...
 * .....
 *
 * After more outputs (1,0, 1,0):
 *
 * .....
 * .....
 * ..^..
 * .##..
 * .....
 *
 * The robot is now back where it started, but because it is now on a white panel, input instructions should be provided
 * 1. After several more outputs (0,1, 1,0, 1,0), the area looks like this:
 *
 * .....
 * ..<#.
 * ...#.
 * .##..
 * .....
 *
 * Before you deploy the robot, you should probably have an estimate of the area it will cover: specifically, you need
 * to know the number of panels it paints at least once, regardless of color. In the example above, the robot painted 6
 * panels at least once. (It painted its starting panel twice, but that panel is still only counted once; it also never
 * painted the panel it ended on.)
 *
 * Build a new emergency hull painting robot and run the Intcode program on it. How many panels does it paint at least
 * once?
 */
enum class PanelColor(val mnemonic: Int, val marker: Char) {
    BLACK(0, ' '),
    WHITE(1, '\u2588')
}

fun Long.toPanelColor() = PanelColor.values().find { it.mnemonic == this.toInt() } ?: throw IllegalArgumentException("$this is not a valid color for this robot")

enum class Direction(val deltaX: Int, val deltaY: Int) {
    NORTH(1, 0),
    SOUTH(-1, 0),
    EAST(0, 1),
    WEST(0, -1)
}

fun Direction.turn(turnDirection: TurnDirection) = when (turnDirection) {
    TurnDirection.LEFT -> when (this) {
        Direction.NORTH -> Direction.WEST
        Direction.SOUTH -> Direction.EAST
        Direction.EAST -> Direction.NORTH
        Direction.WEST -> Direction.SOUTH
    }
    TurnDirection.RIGHT -> when (this) {
        Direction.NORTH -> Direction.EAST
        Direction.SOUTH -> Direction.WEST
        Direction.EAST -> Direction.SOUTH
        Direction.WEST -> Direction.NORTH
    }
}

enum class TurnDirection(val mnemonic: Int) {
    LEFT(0),
    RIGHT(1)
}

fun Long.toTurnDirection() = TurnDirection.values().find { it.mnemonic == this.toInt() } ?: throw IllegalArgumentException("$this is not a valid color for this robot")

class Panel(val x: Int, val y: Int, color: PanelColor) {
    var color: PanelColor = color
        set(value) {
            ++this.paintAmount
            field = value
        }

    private var paintAmount = 0

    val paints get() = this.paintAmount

    override fun toString(): String {
        return "Panel(x=${this.x}, y=${this.y}, color=${this.color}, paintAmount=${this.paintAmount})"
    }
}

class Hull {
    private var hull = mutableListOf<Panel>()
    val panels get() = this.hull.toList()

    operator fun get(x: Int, y: Int) = this.hull.find { it.x == x && it.y == y } ?: Panel(x, y, PanelColor.BLACK).apply { this@Hull.hull.add(this) }
}

class HullPaintingRobot(private val hull: Hull, var x: Int, var y: Int, private val instructions: List<Long>) {
    private var currentDirection = Direction.NORTH
    private val outList = mutableListOf<Long>()

    private fun outManager(long: Long) {
        this.outList += long
        if (this.outList.count() < 2) return
        val (color, turnDirection) = this.outList
        this.outList.clear()
        this.hull[this.x, this.y].color = color.toPanelColor()
        this.currentDirection = this.currentDirection.turn(turnDirection.toTurnDirection())
        this.x += this.currentDirection.deltaX
        this.y += this.currentDirection.deltaY
    }

    fun run() {
        val infiniteSequence = sequence {
            while (true) {
                val currentPanel = this@HullPaintingRobot.hull[this@HullPaintingRobot.x, this@HullPaintingRobot.y]
                yield(currentPanel.color.mnemonic.toLong())
            }
        }
        // Using production from day 9, which is the up-to-date IntCode spec (for now)
        IntCodeComputer.executeSyncProgram(this.instructions, infiniteSequence, this::outManager).let { IntCodeComputer.stop() }
    }
}

fun paintPanelsInHull(instructions: List<Long>, hullInitializingInstruction: Hull.() -> Unit = {}): Hull {
    val hull = Hull().apply(hullInitializingInstruction)
    val robot = HullPaintingRobot(hull, 0, 0, instructions)
    robot.run()
    return hull
}

fun findPaintedPanelsInHull(hull: Hull): List<Panel> {
    return hull.panels.filter { it.paints >= 1 }
}

fun findPaintedPanelsAmount(hull: Hull): Int = findPaintedPanelsInHull(hull).count()

fun main() {
    // 2_018
    println(findPaintedPanelsAmount(paintPanelsInHull(FileReader("d11_p1.txt").read()[0].split(',').map(String::toLong))))
}
