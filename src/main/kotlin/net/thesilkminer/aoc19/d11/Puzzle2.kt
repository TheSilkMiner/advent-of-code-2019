package net.thesilkminer.aoc19.d11

import net.thesilkminer.aoc19.d9.IntCodeComputer
import net.thesilkminer.aoc19.util.FileReader

/*
 * --- Part Two ---
 *
 * You're not sure what it's trying to paint, but it's definitely not a registration identifier. The Space Police are
 * getting impatient.
 *
 * Checking your external ship cameras again, you notice a white panel marked "emergency hull painting robot starting
 * panel". The rest of the panels are still black, but it looks like the robot was expecting to start on a white panel,
 * not a black one.
 *
 * Based on the Space Law Space Brochure that the Space Police attached to one of your windows, a valid registration
 * identifier is always eight capital letters. After starting the robot on a single white panel instead, what
 * registration identifier does it paint on your hull?
 */
fun paintRegistrationIdentifier(instruction: List<Long>) = paintPanelsInHull(instruction) { this[0, 0].color = PanelColor.WHITE }

fun readHullRegistrationIdentifier(hull: Hull) {
    val biggestX = hull.panels.map { it.x }.max()!!
    val biggestY = hull.panels.map { it.y }.max()!!
    val smallestX = hull.panels.map { it.x }.min()!!
    val smallestY = hull.panels.map { it.y }.min()!!
    // I don't know why, okay?
    for (i in biggestX downTo smallestX) {
        for (j in smallestY..biggestY) {
            print(hull[i, j].color.marker)
        }
        println()
    }
}

fun main() {
    // APFKRKBR
    readHullRegistrationIdentifier(paintRegistrationIdentifier(FileReader("d11_p2.txt").read()[0].split(",").map(String::toLong)))
}
