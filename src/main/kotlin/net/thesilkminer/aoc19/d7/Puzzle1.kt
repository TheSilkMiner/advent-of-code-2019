package net.thesilkminer.aoc19.d7

import net.thesilkminer.aoc19.util.FileReader
import kotlin.random.Random

/*
 * --- Day 7: Amplification Circuit ---
 *
 * Based on the navigational maps, you're going to need to send more power to your ship's thrusters to reach Santa in
 * time. To do this, you'll need to configure a series of amplifiers already installed on the ship.
 *
 * There are five amplifiers connected in series; each one receives an input signal and produces an output signal. They
 * are connected such that the first amplifier's output leads to the second amplifier's input, the second amplifier's
 * output leads to the third amplifier's input, and so on. The first amplifier's input value is 0, and the last
 * amplifier's output leads to your ship's thrusters.
 *
 *     O-------O  O-------O  O-------O  O-------O  O-------O
 * 0 ->| Amp A |->| Amp B |->| Amp C |->| Amp D |->| Amp E |-> (to thrusters)
 *     O-------O  O-------O  O-------O  O-------O  O-------O
 *
 * The Elves have sent you some Amplifier Controller Software (your puzzle input), a program that should run on your
 * existing Intcode computer. Each amplifier will need to run a copy of the program.
 *
 * When a copy of the program starts running on an amplifier, it will first use an input instruction to ask the
 * amplifier for its current phase setting (an integer from 0 to 4). Each phase setting is used exactly once, but the
 * Elves can't remember which amplifier needs which phase setting.
 *
 * The program will then call another input instruction to get the amplifier's input signal, compute the correct output
 * signal, and supply it back to the amplifier with an output instruction. (If the amplifier has not yet received an
 * input signal, it waits until one arrives.)
 *
 * Your job is to find the largest output signal that can be sent to the thrusters by trying every possible combination
 * of phase settings on the amplifiers. Make sure that memory is not shared or reused between copies of the program.
 *
 * For example, suppose you want to try the phase setting sequence 3,1,2,4,0, which would mean setting amplifier A to
 * phase setting 3, amplifier B to setting 1, C to 2, D to 4, and E to 0. Then, you could determine the output signal
 * that gets sent from amplifier E to the thrusters with the following steps:
 *
 *     Start the copy of the amplifier controller software that will run on amplifier A. At its first input instruction,
 *     provide it the amplifier's phase setting, 3. At its second input instruction, provide it the input signal, 0.
 *     After some calculations, it will use an output instruction to indicate the amplifier's output signal.
 *
 *     Start the software for amplifier B. Provide it the phase setting (1) and then whatever output signal was produced
 *     from amplifier A. It will then produce a new output signal destined for amplifier C.
 *
 *     Start the software for amplifier C, provide the phase setting (2) and the value from amplifier B, then collect
 *     its output signal.
 *
 *     Run amplifier D's software, provide the phase setting (4) and input value, and collect its output signal.
 *
 *     Run amplifier E's software, provide the phase setting (0) and input value, and collect its output signal.
 *
 * The final output signal from amplifier E would be sent to the thrusters. However, this phase setting sequence may not
 * have been the best one; another sequence might have sent a higher signal to the thrusters.
 *
 * Here are some example programs:
 *
 *     Max thruster signal 43210 (from phase setting sequence 4,3,2,1,0):
 *
 *     3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0
 *
 *     Max thruster signal 54321 (from phase setting sequence 0,1,2,3,4):
 *
 *     3,23,3,24,1002,24,10,24,1002,23,-1,23,
 *     101,5,23,23,1,24,23,23,4,23,99,0,0
 *
 *     Max thruster signal 65210 (from phase setting sequence 1,0,4,3,2):
 *
 *     3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
 *     1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0
 *
 * Try every combination of phase settings on the amplifiers. What is the highest signal that can be sent to the
 * thrusters?
 */
typealias InputBuffer = Iterator<Int>

class HaltException : Exception("The program halted")
class UnknownOpcodeException(opcode: Int) : Exception("The given opcode $opcode is not recognized by an IntCode CPU")
class UnknownParameterTypeException(parameterType: Int) : Exception("The given parameter type $parameterType is not recognized by an IntCode CPU")

class InfiniteList<E : Any>(private val default: E, private val wrappedList: MutableList<E> = mutableListOf()) : MutableList<E> by wrappedList {
    override fun get(index: Int) = try { this.wrappedList[index] } catch (e: Exception) { this.default }
}

enum class ParameterType(val mnemonic: Int, val resolve: (Int, Memory) -> Int) {
    POS(0, { a, b -> b[a] }),
    IMM(1, { a, _ -> a })
}

enum class Opcode(val opcode: Int, val argumentAmount: Int, val operation: (List<Parameter>, GlobalState) -> Unit) {
    SUM(1, 3, { p, gs -> gs.memory[p[2].ensureType(ParameterType.POS).value] = p[0].resolve(gs) + p[1].resolve(gs) }),
    MUL(2, 3, { p, gs -> gs.memory[p[2].ensureType(ParameterType.POS).value] = p[0].resolve(gs) * p[1].resolve(gs) }),
    REA(3, 1, { p, gs -> gs.memory[p[0].ensureType(ParameterType.POS).value] = gs.inputs.next() }),
    WRT(4, 1, { p, gs -> gs.outputStream(p[0].resolve(gs)) }),
    JMT(5, 2, { p, gs -> if (p[0].resolve(gs) != 0) gs.insnPointer = p[1].resolve(gs) else Unit }),
    JMF(6, 2, { p, gs -> if (p[0].resolve(gs) == 0) gs.insnPointer = p[1].resolve(gs) else Unit }),
    LTH(7, 3, { p, gs -> gs.memory[p[2].ensureType(ParameterType.POS).value] = if (p[0].resolve(gs) < p[1].resolve(gs)) 1 else 0 }),
    EQL(8, 3, { p, gs -> gs.memory[p[2].ensureType(ParameterType.POS).value] = if (p[0].resolve(gs) == p[1].resolve(gs)) 1 else 0 }),
    HAL(99, 0, { _, _ -> throw HaltException() })
}

data class Parameter(val value: Int, val parameterType: ParameterType) {
    fun ensureType(type: ParameterType) = this.apply {
        check(this.parameterType == type) { "Mismatching type found: expected $type, found ${this.parameterType}" }
    }
    fun resolve(globalState: GlobalState) = this.resolve(globalState.memory)

    private fun resolve(memory: Memory) = this.parameterType.resolve(this.value, memory)
}

class Memory(private val size: Int) {
    private val contents = IntArray(this.size)

    val dump: String get() = this.contents.toList().toString()

    init {
        this.clear()
    }

    operator fun get(index: Int): Int {
        require(index in 0 until this.size) { "Index $index is outside the bounds of memory 0..${this.size}" }
        return this.contents[index]
    }
    operator fun set(index: Int, value: Int) {
        require(index in 0 until this.size) { "Index $index is outside the bounds of memory 0..${this.size}" }
        this.contents[index] = value
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun fill(from: Int, vararg values: Int) {
        require(from in 0 until this.size) { "Index $from is outside the bounds of memory 0..${this.size}" }
        require(from + values.count() - 1 in 0 until this.size) { "Unable to fill areas outside memory" }
        for (i in 0 until values.count()) this[from + i] = values[i]
    }
    fun fill(from: Int, values: List<Int>) = fill(from, *values.toTypedArray().toIntArray())

    @Suppress("MemberVisibilityCanBePrivate")
    fun clear() = (0 until this.size).forEach { this.contents[it] = Random.Default.nextInt() }
}

class Cpu {
    var isBusy = false

    private fun runInstruction(globalState: GlobalState) {
        val (opcode, paramTypeList) = globalState.memory[globalState.insnPointer++].split()
        val rawParameters = opcode.gatherArgumentsIntoList(globalState)
        val parameters = paramTypeList.matchTo(rawParameters)
        try {
            opcode.operation(parameters, globalState)
        } catch (e: HaltException) {
            throw e
        } catch (e: Exception) {
            globalState.insnPointer -= opcode.argumentAmount + 1
            throw Exception("Unable to execute instruction $opcode $parameters (currently pointing at ${globalState.insnPointer})\nMemory dump: ${globalState.memory.dump}", e)
        }
    }

    fun submit(globalState: GlobalState) {
        try {
            this.isBusy = true
            while (true) {
                this.runInstruction(globalState)
            }
        } catch (e: HaltException) {
            // Program halted
            globalState.insnPointer = -1
        } finally {
            this.isBusy = false
        }
    }

    private fun Opcode.gatherArgumentsIntoList(gs: GlobalState) = (0 until this.argumentAmount).map { gs.memory[gs.insnPointer++] }
    private fun InfiniteList<ParameterType>.matchTo(rawParameters: List<Int>) = rawParameters.mapIndexed { i, p -> Parameter(p, this[i]) }

    private fun Int.split() = Pair((this % 100).toOpcode(), this.toParameterTypeList())
    private fun Int.toOpcode() = Opcode.values().find { it.opcode == this % 100 } ?: throw UnknownOpcodeException(this)
    private fun Int.toParameterType() = ParameterType.values().find { it.mnemonic == this % 10 }
        ?: throw UnknownParameterTypeException(this)

    private fun Int.toParameterTypeList(): InfiniteList<ParameterType> {
        val list = InfiniteList(ParameterType.POS)
        var paramType = this / 100
        while (paramType != 0) {
            list += paramType.toParameterType()
            paramType /= 10
        }
        return list
    }
}

@Suppress("SpellCheckingInspection")
data class GlobalState(val memory: Memory, var insnPointer: Int, val inputs: InputBuffer, val outputStream: (Any) -> Unit)

object IntCodeComputer {
    private val cpuList = listOf(Cpu())

    fun executeProgram(instructions: List<Int>, inputQueue: Sequence<Int> = sequenceOf(), outputStream: (Any) -> Unit = ::println): Memory {
        val globalState = GlobalState(Memory(instructions.count()).apply { this.fill(0, instructions) }, 0, inputQueue.iterator(), outputStream)
        this.cpuList.find { !it.isBusy }?.submit(globalState) ?: throw IllegalStateException("No not busy CPU found")
        return globalState.memory
    }
}

data class Tuple5<T>(val a: T, val b: T, val c: T, val d: T, val e: T)

fun List<Int>.runProgramFor(tuple5: Tuple5<Int>): Int {
    fun List<Int>.runProgramWith(value: Int, input: Int) = mutableListOf<Int>().apply {
        IntCodeComputer.executeProgram(this@runProgramWith, sequenceOf(value, input)) { this += it as Int }
    }[0]
    return this.runProgramWith(tuple5.e, this.runProgramWith(tuple5.d, this.runProgramWith(tuple5.c, this.runProgramWith(tuple5.b, this.runProgramWith(tuple5.a, 0)))))
}

fun Tuple5<Int>.ensureAllDifferent() = !(this.a == this.b || this.a == this.c || this.a == this.d || this.a == this.e
        || this.b == this.c || this.b == this.d || this.b == this.e
        || this.c == this.d || this.c == this.e
        || this.d == this.e)

fun findMaxWithinInputRange(instructions: List<Int>, from: Int, to: Int) : Pair<Tuple5<Int>, Int> {
    val phaseSequences = sequence {
        val r = from..to
        r.forEach { a -> r.forEach { b -> r.forEach { c -> r.forEach { d -> r.forEach { e -> yield(Tuple5(a, b, c, d, e)) } } } } }
    }
    return phaseSequences
        .filter { it.ensureAllDifferent() }
        .map { Pair(it, instructions.runProgramFor(it)) }
        .sortedByDescending { it.second }
        .first()
}

fun main() {
    // 45_730
    println(findMaxWithinInputRange(FileReader("d7_p1.txt").read()[0].split(",").map(String::toInt), 0, 4).second)
}
