package net.thesilkminer.aoc19.d4

import net.thesilkminer.aoc19.util.FileReader

/*
 * --- Part Two ---
 *
 * An Elf just remembered one more important detail: the two adjacent matching digits are not part of a larger group of
 * matching digits.
 *
 * Given this additional criterion, but still ignoring the range rule, the following are now true:
 *
 *     112233 meets these criteria because the digits never decrease and all repeated digits are exactly two digits
 *     long.
 *     123444 no longer meets the criteria (the repeated 44 is part of a larger group of 444).
 *     111122 meets the criteria (even though 1 is repeated more than twice, it still contains a double 22).
 *
 * How many different passwords within the range given in your puzzle input meet all of the criteria?
 */
fun ensureValidCombinationWithNewCriteria(combination: Int): Boolean {
    val stringCombination = combination.toString()
    val regexSeq = sequence {
        for (i in 1..9) {
            val x = (11 * i).toString()
            val c = x[0]
            yield(Pair(x, Regex("^[^$c]*$x[^$c]*\$")))
        }
    }
    return ensureValidCombination(combination) && regexSeq
        .filter { stringCombination.contains(it.first) && stringCombination.matches(it.second) }
        .count() >= 1
}

fun calculateCombinationsWithAdditionalCriteria(min: Int, max: Int)  = sequence { for (i in min..max) yield(i) }
    .map(::ensureValidCombinationWithNewCriteria)
    .filter { it }
    .count()

fun main() {
    // 1_258
    val (min, max) = FileReader("d4_p1.txt").read()[0].split('-').map(String::toInt)
    println(calculateCombinationsWithAdditionalCriteria(min, max))
}
