package net.thesilkminer.aoc19.d4

import net.thesilkminer.aoc19.util.FileReader

/*
 * --- Day 4: Secure Container ---
 *
 * You arrive at the Venus fuel depot only to discover it's protected by a password. The Elves had written the password
 * on a sticky note, but someone threw it out.
 *
 * However, they do remember a few key facts about the password:
 *
 *     It is a six-digit number.
 *     The value is within the range given in your puzzle input.
 *     Two adjacent digits are the same (like 22 in 122345).
 *     Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or
 *     135679).
 *
 * Other than the range rule, the following are true:
 *
 *     111111 meets these criteria (double 11, never decreases).
 *     223450 does not meet these criteria (decreasing pair of digits 50).
 *     123789 does not meet these criteria (no double).
 *
 * How many different passwords within the range given in your puzzle input meet these criteria?
 */
fun ensureValidCombination(combination: Int): Boolean {
    // six-digit number, but we choose 111_111 because of other requirements)
    if (combination !in 111_111..999_999) return false
    val combinationAsString = combination.toString()
    // contains at least a double digit pair
    if (sequence { for (i in 1..9) yield(11 * i) }.filter { combinationAsString.contains(it.toString()) }.count() == 0) return false
    for (i in 0 until combinationAsString.count() - 1) {
        if (combinationAsString[i].toInt() > combinationAsString[i + 1].toInt()) return false
    }
    return true
}

fun calculateCombinations(min: Int, max: Int) = sequence { for (i in min..max) yield(i) }
    .map(::ensureValidCombination)
    .filter { it }
    .count()

fun main() {
    // 1_864
    val (min, max) = FileReader("d4_p1.txt").read()[0].split('-').map(String::toInt)
    println(calculateCombinations(min, max))
}
