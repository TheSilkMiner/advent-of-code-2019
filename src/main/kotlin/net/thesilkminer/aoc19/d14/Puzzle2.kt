package net.thesilkminer.aoc19.d14

import net.thesilkminer.aoc19.util.FileReader
import java.util.*

/*
 * --- Part Two ---
 *
 * After collecting ORE for a while, you check your cargo hold: 1 trillion (1000000000000) units of ORE.
 *
 * With that much ore, given the examples above:
 *
 *     The 13312 ORE-per-FUEL example could produce 82892753 FUEL.
 *     The 180697 ORE-per-FUEL example could produce 5586022 FUEL.
 *     The 2210736 ORE-per-FUEL example could produce 460664 FUEL.
 *
 * Given 1 trillion ORE, what is the maximum amount of FUEL you can produce?
 */

/*
fun main() {
    val reactions = Helper.getResourceAsStream("/d14_p2.txt")
        .bufferedReader()
        .useLines { lines ->
            lines.map { line ->
                val (inStr, outStr) = line.split(" => ")
                val inputs = inStr
                    .split(", ").map { it.split(' ').let { (quantity, chemical) -> chemical to quantity.toInt() } }
                val output = outStr.split(' ').let { (quantity, chemical) -> chemical to quantity.toInt() }
                output.first to (output.second to inputs)
            }.toMap()
        }

    fun calculateRequiredOre(fuelQuantity: Long): Long {
        var requiredOre = 0L
        val leftovers = mutableMapOf<String, Long>()
        val queue: Queue<Pair<String, Long>> = LinkedList()

        queue.add("FUEL" to fuelQuantity)
        while (queue.isNotEmpty()) {
            val (requiredChemical, requiredQuantity) = queue.remove()

            if (requiredChemical == "ORE") {
                requiredOre += requiredQuantity
                continue
            }

            val leftoverQuantity = leftovers.getOrDefault(requiredChemical, 0)
            when {
                leftoverQuantity < requiredQuantity -> {
                    leftovers.remove(requiredChemical)
                    val quantityToProduce = requiredQuantity - leftoverQuantity
                    val (producedQuantity, inputChemicals) = reactions.getValue(requiredChemical)

                    val batchesRequired = (quantityToProduce + producedQuantity - 1) / producedQuantity
                    if (producedQuantity * batchesRequired > quantityToProduce) {
                        leftovers[requiredChemical] = producedQuantity * batchesRequired - quantityToProduce
                    }
                    inputChemicals.forEach { (inputChemical, inputQuantity) ->
                        queue.add(inputChemical to inputQuantity * batchesRequired)
                    }
                }
                leftoverQuantity == requiredQuantity -> {
                    leftovers.remove(requiredChemical)
                }
                leftoverQuantity > requiredQuantity -> {
                    leftovers[requiredChemical] = leftoverQuantity - requiredQuantity
                }
            }
        }
        return requiredOre
    }

    val answer1 = calculateRequiredOre(fuelQuantity = 1)
    println("Part 1: $answer1")

    val answer2 = run {
        val totalOre = 1_000_000_000_000

        var (upperBound, upperRequiredOre) = generateSequence(1L) { it * 2 }
            .map { it to calculateRequiredOre(fuelQuantity = it) }
            .dropWhile { it.second < totalOre }
            .firstOrNull() ?: return@run 0

        if (upperRequiredOre == totalOre) {
            return@run upperBound
        }

        var lowerBound = upperBound / 2
        while (lowerBound + 1 < upperBound) {
            val targetFuel = (upperBound + lowerBound) / 2

            val requiredOre = calculateRequiredOre(fuelQuantity = targetFuel)
            when {
                requiredOre < totalOre -> lowerBound = targetFuel
                requiredOre == totalOre -> return@run targetFuel
                requiredOre > totalOre -> upperBound = targetFuel
            }
        }
        return@run lowerBound
    }
    println("Part 2: $answer2")
}
 */
// TODO: Figure out why the code above works
// TODO: What is the difference???
// Anyway: source for the above is https://www.reddit.com/r/adventofcode/comments/eafj32/2019_day_14_solutions/faua1uu/

fun CraftingEnvironment.findOre(ore: WeightedIngredient, fuel: WeightedIngredient): Long {
    var requiredOre = 0L
    val queue: Queue<WeightedIngredient> = LinkedList()
    queue.add(fuel)
    while (queue.isNotEmpty()) {
        val required = queue.remove()!!
        if (required.ingredient == ore.ingredient) {
            requiredOre += required.weight
            continue
        }

        val leftOver = this.pool.getValue(required.ingredient)
        when {
            leftOver < required.weight -> {
                this.pool[required.ingredient] = 0
                val quantityToProduce = required.weight - leftOver
                val recipe = this.recipes.find { required.ingredient == it.output.ingredient }!!

                val batches = (quantityToProduce + recipe.output.weight - 1) / recipe.output.weight
                if (recipe.output.weight * batches > quantityToProduce) {
                    this.pool[required.ingredient] = recipe.output.weight * batches - quantityToProduce
                }
                recipe.inputs.forEach { queue.add(WeightedIngredient(it.ingredient, it.weight * batches.toInt())) }
            }
            leftOver == required.weight.toLong() -> {
                this.pool[required.ingredient] = 0
            }
            leftOver > required.weight -> {
                this.pool[required.ingredient] = leftOver - required.weight
            }
        }
    }
    return requiredOre
}

fun CraftingEnvironment.findFuelForOres(ore: WeightedIngredient, fuel: WeightedIngredient): Long {
    var (upperBound, upperRequiredOre) = generateSequence(1L) { it * 2 }
        .map { it to this.findOre(ore, WeightedIngredient(fuel.ingredient, it.toInt())) }
        .dropWhile { it.second < ore.weight }
        .firstOrNull() ?: return 0L

    if (upperRequiredOre == ore.weight.toLong()) {
        return upperBound
    }

    var lowerBound = upperBound / 2
    while (lowerBound + 1 < upperBound) {
        val targetFuel = (upperBound + lowerBound) / 2

        val requiredOre = this.findOre(ore, WeightedIngredient(fuel.ingredient, targetFuel.toInt()))
        when {
            requiredOre < ore.weight.toLong() -> lowerBound = targetFuel
            requiredOre == ore.weight.toLong() -> return targetFuel
            requiredOre > ore.weight.toLong() -> upperBound = targetFuel
        }
    }
    return lowerBound
}

fun findAmountPerGiven(recipesData: List<String>, oreAmount: Long): Long {
    val materials = mutableSetOf<Ingredient>()
    val recipes = mutableSetOf<Recipe>()
    recipesData.asSequence().map(String::toRecipe).forEach {
        recipes += it
        materials += it.output.ingredient
        it.inputs.asSequence().map { material -> material.ingredient }.forEach { material -> materials += material }
    }
    val environment = CraftingEnvironment(materials, recipes)
    val ore = materials.find { it.descriptor == "ORE" }!!
    val fuel = materials.find { it.descriptor == "FUEL" }!!
    return environment.findFuelForOres(WeightedIngredient(ore, oreAmount.toInt()), WeightedIngredient(fuel, 1))
}

fun main() {
    // 3_412_429
    println(findAmountPerGiven(FileReader("d14_p2.txt").read(), 1_000_000_000_000L))
}
