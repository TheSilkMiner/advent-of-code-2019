package net.thesilkminer.aoc19.d6

import net.thesilkminer.aoc19.util.FileReader

/*
 * --- Part Two ---
 *
 * Now, you just need to figure out how many orbital transfers you (YOU) need to take to get to Santa (SAN).
 *
 * You start at the object YOU are orbiting; your destination is the object SAN is orbiting. An orbital transfer lets
 * you move from any object to an object orbiting or orbited by that object.
 *
 * For example, suppose you have the following map:
 *
 * COM)B
 * B)C
 * C)D
 * D)E
 * E)F
 * B)G
 * G)H
 * D)I
 * E)J
 * J)K
 * K)L
 * K)YOU
 * I)SAN
 *
 * Visually, the above map of orbits looks like this:
 *
 *                           YOU
 *                          /
 *         G - H       J - K - L
 *        /           /
 * COM - B - C - D - E - F
 *                \
 *                 I - SAN
 *
 * In this example, YOU are in orbit around K, and SAN is in orbit around I. To move from K to I, a minimum of 4 orbital
 * transfers are required:
 *
 *     K to J
 *     J to E
 *     E to D
 *     D to I
 *
 * Afterward, the map of orbits looks like this:
 *
 *         G - H       J - K - L
 *        /           /
 * COM - B - C - D - E - F
 *                \
 *                 I - SAN
 *                  \
 *                   YOU
 *
 * What is the minimum number of orbital transfers required to move from the object YOU are orbiting to the object SAN
 * is orbiting? (Between the objects they are orbiting - not between YOU and SAN.)
 */
fun OrbitingObject.buildSubMap(): UniversalOrbitMap {
    val orbitingObjectsList = mutableListOf(this)
    var parent = this.directOrbit
    while (parent != null) {
        orbitingObjectsList += parent
        parent = parent.directOrbit
    }
    val rebuildList = orbitingObjectsList.filter { it.directOrbit != null }.map { "${it.directOrbit!!.name})${it.name}" }
    return UniversalOrbitMap(rebuildList)
}

fun UniversalOrbitMap.calculateLastIntersectionWith(that: UniversalOrbitMap): OrbitingObject {
    var a = this.centerOfMass
    var b = that.centerOfMass
    while (a.name == b.name) {
        if (a.directOrbits.isEmpty() || b.directOrbits.isEmpty()) return a
        a = a.directOrbits[0]
        b = b.directOrbits[0]
    }
    return a.directOrbit!!
}

fun calculateStepsForOrbitalTransferBetween(from: OrbitingObject, to: OrbitingObject): Int {
    fun OrbitingObject.walkUpTo(d: OrbitingObject) = sequence {
        var a = this@walkUpTo
        while (a.name != d.name) {
            yield(a)
            a = a.directOrbit!!
        }
        yield(a)
    }

    val fromSubMap = from.buildSubMap()
    val toSubMap = to.buildSubMap()
    val intersection = fromSubMap.calculateLastIntersectionWith(toSubMap)
    val stepsFromFromToIntersection = from.walkUpTo(intersection).count()
    val stepsFromToToIntersection = to.walkUpTo(intersection).count()
    return stepsFromFromToIntersection + stepsFromToToIntersection - 2 // we are counting the starting point too
}

fun calculateStepsForOrbitalTransfer(map: UniversalOrbitMap): Int {
    return calculateStepsForOrbitalTransferBetween(map.find("YOU")!!.directOrbit!!, map.find("SAN")!!.directOrbit!!)
}

fun main() {
    // 241
    println(calculateStepsForOrbitalTransfer(UniversalOrbitMap(FileReader("d6_p2.txt").read())))
}
