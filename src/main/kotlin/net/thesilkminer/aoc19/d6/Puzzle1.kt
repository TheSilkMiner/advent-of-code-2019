package net.thesilkminer.aoc19.d6

import net.thesilkminer.aoc19.util.FileReader
import java.lang.IllegalStateException

/*
 * --- Day 6: Universal Orbit Map ---
 *
 * You've landed at the Universal Orbit Map facility on Mercury. Because navigation in space often involves transferring
 * between orbits, the orbit maps here are useful for finding efficient routes between, for example, you and Santa. You
 * download a map of the local orbits (your puzzle input).
 *
 * Except for the universal Center of Mass (COM), every object in space is in orbit around exactly one other object. An
 * orbit looks roughly like this:
 *
 *                   \
 *                    \
 *                     |
 *                     |
 * AAA--> o            o <--BBB
 *                     |
 *                     |
 *                    /
 *                   /
 *
 * In this diagram, the object BBB is in orbit around AAA. The path that BBB takes around AAA (drawn with lines) is only
 * partly shown. In the map data, this orbital relationship is written AAA)BBB, which means "BBB is in orbit around
 * AAA".
 *
 * Before you use your map data to plot a course, you need to make sure it wasn't corrupted during the download. To
 * verify maps, the Universal Orbit Map facility uses orbit count checksums - the total number of direct orbits (like
 * the one shown above) and indirect orbits.
 *
 * Whenever A orbits B and B orbits C, then A indirectly orbits C. This chain can be any number of objects long: if A
 * orbits B, B orbits C, and C orbits D, then A indirectly orbits D.
 *
 * For example, suppose you have the following map:
 *
 * COM)B
 * B)C
 * C)D
 * D)E
 * E)F
 * B)G
 * G)H
 * D)I
 * E)J
 * J)K
 * K)L
 *
 * Visually, the above map of orbits looks like this:
 *
 *         G - H       J - K - L
 *        /           /
 * COM - B - C - D - E - F
 *                \
 *                 I
 *
 * In this visual representation, when two objects are connected by a line, the one on the right directly orbits the one
 * on the left.
 *
 * Here, we can count the total number of orbits as follows:
 *
 *     D directly orbits C and indirectly orbits B and COM, a total of 3 orbits.
 *     L directly orbits K and indirectly orbits J, E, D, C, B, and COM, a total of 7 orbits.
 *     COM orbits nothing.
 *
 * The total number of direct and indirect orbits in this example is 42.
 *
 * What is the total number of direct and indirect orbits in your map data?
 */
class OrbitingObject(val name: String) {
    val directOrbits = mutableListOf<OrbitingObject>()
    var directOrbit = null as OrbitingObject?
    var depthId = -1

    override fun toString(): String {
        return "OrbitingObject(name='${this.name}', depthId=${this.depthId}, directOrbits=${this.directOrbits}, directOrbit=${this.directOrbit})"
    }
}

class UniversalOrbitMap(val centerOfMass: OrbitingObject) {
    companion object {
        operator fun invoke(data: List<String>): UniversalOrbitMap {
            val orbitingObjects = mutableMapOf<String, OrbitingObject>()
            data.forEach {
                val (center, orbiting) = it.split(')')
                val centerObject = orbitingObjects.computeIfAbsent(center) { OrbitingObject(center) }
                val orbitingObject = orbitingObjects.computeIfAbsent(orbiting) { OrbitingObject(orbiting) }
                if (orbitingObject.directOrbit != null) {
                    throw IllegalStateException("Orbiting object '$orbiting' is already in orbit around '${orbitingObject.directOrbit!!.name}', but map says it is orbiting around '$center'")
                }
                centerObject.directOrbits += orbitingObject.apply { directOrbit = centerObject }
            }
            val centerOfMass = orbitingObjects.asSequence().map { it.value }.filter { it.directOrbit == null }.apply {
                if (this.count() != 1) throw IllegalStateException("There is more than once center of mass: this is illegal!")
            }.first()
            return UniversalOrbitMap(centerOfMass)
        }
    }

    init {
        this.rebuildDepth()
    }

    private fun rebuildDepth() {
        fun rebuildDepth(level: Int, current: OrbitingObject) {
            current.depthId = level
            current.directOrbits.forEach { rebuildDepth(level + 1, it) }
        }

        rebuildDepth(0, this.centerOfMass)
    }

    fun find(name: String): OrbitingObject? {
        fun OrbitingObject.findOrLoop(name: String): OrbitingObject? {
            if (this.name == name) return this
            return this.directOrbits.map { it.findOrLoop(name) }.firstOrNull { it != null }
        }
        return this.centerOfMass.findOrLoop(name)
    }
}

fun calculateTotalOrbits(map: UniversalOrbitMap): Int {
    fun calculateTotalOrbits(orbitingObject: OrbitingObject): Int {
        return orbitingObject.depthId + orbitingObject.directOrbits.map { calculateTotalOrbits(it) }.sum()
    }
    return calculateTotalOrbits(map.centerOfMass)
}

fun main() {
    // 158_090
    println(calculateTotalOrbits(UniversalOrbitMap(FileReader("d6_p1.txt").read())))
}
